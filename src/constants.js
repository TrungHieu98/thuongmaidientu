// Auth\______________________________________________
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const LOGOUT = 'LOGOUT'
export const LOGIN_SUCCESS = 'LOGIN'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_BY_ADMIN = 'LOGIN_BY_ADMIN'
export const USE_PROMO = 'USE_PROMO'
export const RECORD_ORDER_OF_USER = 'RECORD_ORDER_OF_USER'
export const RECORD_ORDER_OF_USER_FAIL = 'RECORD_ORDER_OF_USER_FAIL'

// Admin\______________________________________________
export const ADD_ADMIN_SUCCESS = 'ADD_ADMIN_SUCCESS'
export const ADD_ADMIN_FAIL = 'ADD_ADMIN_FAIL'
export const GET_ADMINS = 'GET_ADMINS'
export const GET_ADMINS_FAIL = 'GET_ADMIN_FAIL'
export const ADMINS_LOADING = 'ADMINS_LOADING'
export const DELETE_ADMIN = 'DELETE_ADMIN'
export const DELETE_ADMIN_FAIL = 'DELETE_ADMIN_FAIL'

// Product\______________________________________________
export const GET_PRODUCTS_BY_ADMIN = 'GET_PRODUCTS_BY_ADMIN'
export const GET_PRODUCTS_FAIL = 'GET_PRODUCTS_FAIL'
export const GET_PRODUCT = 'GET_PRODUCT'
export const GET_PRODUCT_FAIL = 'GET_PRODUCT_FAIL'
export const PRODUCTS_LOADING = 'PRODUCTS_LOADING'
export const DELETE_PRODUCT = 'DELETE_PRODUCT'
export const DELETE_PRODUCT_FAIL = 'DELETE_PRODUCT_FAIL'
export const CREATE_PRODUCT = 'CREATE_PRODUCT'
export const CREATE_PRODUCT_FAIL = 'CREATE_PRODUCT_FAIL'
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT'
export const UPDATE_PRODUCT_FAIL = 'UPDATE_PRODUCT_FAIL'
export const GET_PRODUCTS_WITH_FILTER = 'GET_PRODUCTS_WITH_FILTER'
export const GET_PRODUCTS_WITH_FILTER_FAIL = 'GET_PRODUCTS_WITH_FILTER_FAIL'
export const GET_PRODUCTS_BY_USER = 'GET_PRODUCTS_BY_USER'
export const GET_PRODUCTS_BY_USER_FAIL = 'GET_PRODUCTS_BY_USER_FAIL'

// User\______________________________________________
export const USER_LOADED = 'USER_LOADED'
export const GET_USERS = 'GET_USERS'
export const GET_USERS_FAIL = 'GET_USERS_FAIL'
export const GET_USER = 'GET_USER'
export const GET_USER_FAIL = 'GET_USER_FAIL'
export const USERS_LOADING = 'USERS_LOADING'
export const DELETE_USER = 'DELETE_USER'
export const DELETE_USER_FAIL = 'DELETE_USER_FAIL'
export const UPDATE_USER = 'UPDATE_USER'
export const UPDATE_USER_FAIL = 'UPDATE_USER_FAIL'


// Error\______________________________________________
export const GET_ERRORS = 'GET_ERRORS'
export const CLEAR_ERRORS = 'CLEAR_ERRORS'

// ProductType\________________________________________
export const GET_ALL_TYPE = 'GET_ALL_TYPE'
export const GET_TYPE = 'GET_TYPE'
export const GET_TYPES_FAIL = 'GET_TYPES_FAIL'
export const GET_TYPE_FAIL = 'GET_TYPE_FAIL'
export const TYPES_LOADING = 'TYPES_LOADING'
export const DELETE_TYPE = 'DELETE_TYPE'
export const DELETE_TYPE_FAIL = 'DELETE_TYPE_FAIL'
export const CREATE_TYPE = 'CREATE_TYPE'
export const CREATE_TYPE_FAIL = 'CREATE_TYPE_FAIL'
export const UPDATE_TYPE = 'UPDATE_TYPE'
export const UPDATE_TYPE_FAIL = 'UPDATE_TYPE_FAIL'
export const GET_TYPES_EXISTED = 'GET_TYPES_EXISTED'
export const GET_TYPES_EXISTED_FAIL = 'GET_TYPES_EXISTED_FAIL'

//Promo\_______________________________________________
export const GET_PROMO = 'GET_PROMO'
export const GET_PROMO_FAIL = 'GET_PROMO_FAIL'
export const GET_PROMOS_BY_ADMIN = 'GET_PROMOS_BY_ADMIN'
export const GET_PROMOS_BY_ADMIN_FAIL = 'GET_PROMOS_BY_ADMIN_FAIL'
export const GET_PROMOS_BY_USER = 'GET_PROMOS_BY_USER'
export const GET_PROMOS_BY_USER_FAIL = 'GET_PROMOS_BY_USER_FAIL'
export const PROMOS_LOADING = 'PROMOS_LOADING'
export const DELETE_PROMO = 'DELETE_PROMO'
export const DELETE_PROMO_FAIL = 'DELETE_PROMO_FAIL'
export const CREATE_PROMO = 'CREATE_PROMO'
export const CREATE_PROMO_FAIL = 'CREATE_PROMO_FAIL'
export const UPDATE_PROMO = 'UPDATE_PROMO'
export const UPDATE_PROMO_FAIL = 'UPDATE_PROMO_FAIL'
export const GET_PROMOS_OF_USER = 'GET_PROMOS_OF_USER'
export const GET_PROMOS_OF_USER_FAIL = 'GET_PROMOS_OF_USER_FAIL'
export const SET_PROMO_SELECTED = 'PROMO_SELECTED'
export const TAKE_PROMO = 'TAKE_PROMO'
export const TAKE_PROMO_FAIL = 'TAKE_PROMO_FAIL'

//Form\_______________________________________________
export const SET_UPDATING = 'SET_UPDATING'
export const SET_CREATING = 'SET_CREATING'

//Cart\_______________________________________________
export const GET_ITEMS = 'GET_ITEMS'
export const ADD_ITEM = 'ADD_ITEM'
export const DELETE_ITEM = 'DELETE_ITEM'
export const FETCH_ORDER_DETAILS = 'FETCH_ORDER_DETAILS'
export const FETCH_ORDER_DETAILS_FAIL = 'FETCH_ORDER_DETAIL_FAIL'
export const CART_LOADING = 'CART_LOADING'
export const CART_CONFIRMED = 'CART_CONFIRMED'
export const SET_ORDER = 'SET_ORDER'
export const SET_PROVISONAL_SUM = 'SET_PROVISONAL_SUM'
export const SET_QUANTITY = 'SET_QUANTITY'
export const CLEAR_LOCAL_STORAGE = 'CLEAR_LOCAL_STORAGE'


//Order\_______________________________________________
export const CREATE_ORDER = 'CREATE_ORDER'
export const CREATE_ORDER_FAIL = 'CREATE_ORDER_FAIL'
export const GET_ORDERS = 'GET_ORDERS'
export const GET_ORDERS_FAIL = 'GET_ORDERS_FAIL'
export const GET_ORDER = 'GET_ORDER'
export const UPDATE_ORDER = 'UPDATE_ORDER'
export const ORDER_LOADING = 'ORDER_LOADING'
export const DELETE_ORDER = 'DELETE_ORDER'
export const GET_ORDERS_HISTORY = 'GET_ORDERS_HISTORY'
export const GET_ORDERS_HISTORY_FAIL = 'GET_ORDERS_HISTORY_FAIL'
export const GET_ORDER_DETAILS_FROM_ORDER = 'GET_ORDER_DETAILS_FROM_ORDER'
export const GET_ORDER_DETAILS_FROM_ORDER_FAIL = 'GET_ORDER_DETAILS_FROM_ORDER_FAIL'

//OrderDetail\_______________________________________________
export const CREATE_ORDER_DETAIL = 'CREATE_ORDER_DETAIL'
export const CREATE_ORDER_DETAIL_FAIL = 'CREATE_ORDER_DETAIL_FAIL'

//Bill\________________________________________________
export const SET_DISCOUNT_VALUE = 'SET_DISCOUNT_VALUE'
export const SET_TOTAL = 'SET_TOTAL'
export const RESET_DISCOUNT_VALUE = 'RESET_DISCOUNT_VALUE'