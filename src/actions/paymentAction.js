import axios from 'axios'

export const createBill = ({ user_id, orderDate, shipDate, receiverAddress, note, orderDetails }) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const order = {
        user: user_id,
        orderDate: orderDate,
        shipDate: shipDate,
        receiverAddress: receiverAddress,
        note: note,
        orderDetails: orderDetails
    }
    const body = JSON.stringify(order)
    

}