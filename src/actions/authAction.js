import axios from 'axios';
import {
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    LOGOUT,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGIN_BY_ADMIN,
    USER_LOADED,
    USE_PROMO,
    TAKE_PROMO,
    TAKE_PROMO_FAIL,
    RECORD_ORDER_OF_USER,
    RECORD_ORDER_OF_USER_FAIL,
    GET_ORDERS_FROM_USER,
    GET_ORDERS_FROM_USER_FAIL,
    CLEAR_AUTH_PROPS,
    // PROMO_NOT_DUPICATE,
    // PROMO_DUPICATED
} from '../constants';
import { returnErrors } from './errorAction';

const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const loadUser = () => (dispatch, getState) => {
    // dispatch({ type: USER_LOADING });
    const userId = {
        userId: getState().auth.userId
    }

    // console.log('from load user', userId)

    // headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    }
    const body = JSON.stringify(userId);
    // console.log('body: ', body)
    axios.post(`${url_host || url_local}/api/user/auth/load`, body, config)
        .then(res => {
            dispatch({
                type: USER_LOADED,
                payload: res.data
            })
            // console.log(res.data)
        })
        .catch(err => {
            console.log('loi load user do chua dang nhap');
            // // dispatch(returnErrors(err.response.data, err.response.status));
            // dispatch({
            //     type: AUTH_ERROR
            // })
        })
}

// register user
export const register = (userInfo) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify(userInfo);
    // console.log('from register action: ', body)

    // axios.get('http://localhost:8000/api/user/get/email', body.email, config)
    //     .then(
    //         console.log('email da ton tai')
    //     )
    //     .catch(
    //         () => {
    axios.post(`${url_host || url_local}/api/user/auth/register`, body, config)
        .then(res => dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        }))
        //.then(console.log('save thanh cong'))
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'REGISTER_FAIL'))
            dispatch({
                type: REGISTER_FAIL
            })
        })
}

export const login = ({ email, password }) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify({ email, password });
    // console.log('from login action: ', body)

    axios.post(`${url_host || url_local}/api/user/auth/login`, body, config)
        .then(res => {
            if (res.data.priority === 1) {
                dispatch({
                    type: LOGIN_BY_ADMIN,
                    payload: res.data
                })
                alert('Bạn đang đăng nhập quyền admin')
            }
            else
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: res.data
                })
        })
        .catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status, 'LOGIN_FAIL'))
            dispatch({
                type: LOGIN_FAIL
            })
        })
}

export const logout = () => dispatch => {
    dispatch({
        type: LOGOUT
    })
}


export const removePromoUsed = (promo_id, user) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        promo_id: promo_id,
        user: user
    }
    const body = JSON.stringify(reqData)
    axios.put(`${url_host || url_local}/api/user/usepromo`, body, config)
        .then(res => {
            dispatch({
                type: USE_PROMO,
                payload: res.data
            })
            console.log(res.data)
        })
        .catch(err => {
            console.log(err)
        })
}

export const recordOrderOfUser = (order_id, user_id) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        order_id: order_id,
        user_id: user_id
    }
    console.log(reqData)
    const body = JSON.stringify(reqData)
    axios.post(`${url_host || url_local}/api/user/recordorder`, body, config)
        .then(res => {
            dispatch({
                type: RECORD_ORDER_OF_USER,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err)
            dispatch({
                type: RECORD_ORDER_OF_USER_FAIL,
            })
        })
}