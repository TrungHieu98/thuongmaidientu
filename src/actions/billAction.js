import { SET_DISCOUNT_VALUE, SET_TOTAL } from '../constants'

export const calculateAndSetDiscount = (promoSelected, provisionalSum) => dispatch => {
    const { percent, minOrderPrice, maxDiscount } = promoSelected
    if (provisionalSum >= minOrderPrice) {
        let discount = provisionalSum / 100 * percent
        if (discount > maxDiscount)
            discount = maxDiscount
        dispatch({
            type: SET_DISCOUNT_VALUE,
            payload: discount.toFixed(2)
        })
        return discount.toFixed(2)
    }
    else {
        alert('Khuyễn mãi chỉ áp dụng cho đơn hàng tối thiểu ' + minOrderPrice)
        dispatch({
            type: SET_DISCOUNT_VALUE,
            payload: 0
        })
        return 0
    }
}

export const setDiscountValue = (discount) => dispatch => {
    dispatch({
        type: SET_DISCOUNT_VALUE,
        payload: discount
    })
    return discount
}

export const setTotal = (discount, provisionalSum) => dispatch => {
    const total = provisionalSum - discount
    dispatch({
        type: SET_TOTAL,
        payload: total
    })
    return total
}