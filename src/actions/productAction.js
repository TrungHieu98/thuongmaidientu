import axios from 'axios';
import {
    GET_PRODUCTS_BY_ADMIN,
    GET_PRODUCTS_FAIL,
    GET_PRODUCT,
    GET_PRODUCT_FAIL,
    DELETE_PRODUCT,
    DELETE_PRODUCT_FAIL,
    CREATE_PRODUCT,
    CREATE_PRODUCT_FAIL,
    UPDATE_PRODUCT,
    UPDATE_PRODUCT_FAIL,
    PRODUCTS_LOADING,
    ADD_ITEM,
    GET_PRODUCTS_WITH_FILTER,
    GET_PRODUCTS_BY_USER,
    GET_PRODUCTS_BY_USER_FAIL
} from '../constants';
import { returnErrors } from './errorAction';

const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const getProductsByAdmin = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch(setProductLoading());
        axios.get(`${url_host || url_local}/api/product`)
            .then(res => {
                console.log('from product action', res.data)
                dispatch({
                    type: GET_PRODUCTS_BY_ADMIN,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                console.log('loi action')
                //spatch(returnErrors(err.response.data, err.response.status, 'GET_DRIVERS_FAIL'));
                // dispatch({
                //     type: GET_ORDERS_FAIL
                // });
                reject(err)
            })
    })
}

export const getProductsByUser = () => dispatch => {
    dispatch(setProductLoading())
    return axios.post(`${url_host || url_local}/api/product/getproducts`)
        .then(res => {
            dispatch({
                type: GET_PRODUCTS_BY_USER,
                payload: res.data
            })
            return res.data
        })
        .catch(err => {
            //spatch(returnErrors(err.response.data, err.response.status, 'GET_DRIVERS_FAIL'));
            dispatch({
                type: GET_PRODUCTS_BY_USER_FAIL
            })
            return err
        })
}

export const setProductLoading = () => {
    return {
        type: PRODUCTS_LOADING
    }
}

export const deleteProduct = (product_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.put(`${url_host || url_local}/api/product/delete/${product_id}`)
            .then(res => {
                dispatch({
                    type: DELETE_PRODUCT
                })
                resolve()
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_PRODUCT_FAIL'))
                dispatch({
                    type: DELETE_PRODUCT_FAIL
                })
                reject()
            })
    })
}

export const createProduct = (product) => (dispatch) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify(product);
    axios.post(`${url_host || url_local}/api/product/create`, body, config)
        .then(res => {
            console.log(res.data)
            dispatch({
                type: CREATE_PRODUCT,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'CREATE_PRODUCT_FAIL'))
            dispatch({
                type: CREATE_PRODUCT_FAIL
            });
        })
}

export const getProduct = (_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.get(`${url_host || url_local}/api/product/${_id}`)
            .then(res => {
                // console.log('from productAction: ', typeof(res.data))

                dispatch({
                    type: GET_PRODUCT,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'GET_PRODUCT_FAIL'))
                dispatch({
                    type: GET_PRODUCT_FAIL
                })
                reject(err)
            })
    })
}

export const updateProduct = (_id, product) => (dispatch) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify(product);

    axios.put(`${url_host || url_local}/api/product/update/${_id}`, body, config)
        .then(res => {
            dispatch({
                type: UPDATE_PRODUCT,
            })
        })
        .catch(err => {
            console.log('loi update')
            //dispatch ...
        })
}

export const getProductWithFilter = (filter, productList) => async dispatch => {

    const products = productList.filter(product => {
        return product.productName.indexOf(filter) > -1
    })
    console.log(products)
    dispatch({
        type: GET_PRODUCTS_WITH_FILTER,
        payload: products
    })
    return products





    // const config = {
    //     headers: {
    //         'Content-Type': 'application/json'
    //     }
    // }

    // const data = {
    //     filter: filter
    // }

    // const body = JSON.stringify(data);
    // console.log('body:', body)

    // axios.post(`${url_host || url_local}/api/product/filter`, body, config)
    //     .then(res => {
    //         console.log('products filtered: ', res.data)
    //         dispatch({
    //             type: GET_PRODUCTS_WITH_FILTER,
    //             payload: res.data
    //         })
    //     })
    //     .catch(err => {
    //         console.log('loi search')
    //         //dispatch ...
    //     })
}
