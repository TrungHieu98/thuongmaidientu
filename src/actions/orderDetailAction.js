import axios from 'axios'
import { CREATE_ORDER_DETAIL, CREATE_ORDER_DETAIL_FAIL } from '../constants'
const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const createOrderDetails = (orderDetailsInCart, order) => dispatch => {
    return new Promise((resolve, reject) => {
        let promises = []
        orderDetailsInCart.forEach(orderDetail => {
            const { product, unitPrice, quantity } = orderDetail
            const p = Promise.resolve(
                createOrderDetail({ product, unitPrice, quantity, order })
            )
            promises.push(p)
        })

        Promise.all(promises)
            .then(orderDetails => {
                dispatch({
                    type: CREATE_ORDER_DETAIL
                })
                // return orderDetails
                resolve(orderDetails)
            })
            .catch(err => {
                dispatch({
                    type: CREATE_ORDER_DETAIL_FAIL
                })
                reject(err)
            })
    })
}

const createOrderDetail = ({ product, unitPrice, quantity, order }) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const data = {
            product: product,
            unitPrice: unitPrice,
            quantity: quantity,
            order: order
        }
        const body = JSON.stringify(data)
        // console.log(body)
        axios.post(`${url_host || url_local}/api/orderdetail/create/`, body, config)
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err)
            })
    })
}