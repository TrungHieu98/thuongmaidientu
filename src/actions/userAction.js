import axios from 'axios'

import {
    GET_USERS,
    GET_USERS_FAIL,
    DELETE_USER,
    DELETE_USER_FAIL,
    USERS_LOADING,
    UPDATE_USER,
    UPDATE_USER_FAIL,
    GET_USER,
    GET_USER_FAIL,
} from '../constants'

import { returnErrors } from './errorAction';
const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const getUsers = () => dispatch => {
    return new Promise((resolve, reject) => {
        dispatch(setUserLoading());
        axios.get(`${url_host || url_local}/api/user`)
            .then(res => {
                console.log('from user action: ', res.data)
                dispatch({
                    type: GET_USERS,
                    payload: res.data
                });
                resolve()
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'GET_USERS_FAIL'))
                dispatch({
                    type: GET_USERS_FAIL
                })
                reject()
            })
    })
}

export const setUserLoading = () => {
    return {
        type: USERS_LOADING
    }
}

export const deleteUser = (user_id) => (dispatch) => {

    axios.put(`${url_host || url_local}/api/user/delete/${user_id}`)
        .then(res => {
            dispatch({
                type: DELETE_USER
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_USER_FAIL'))
            dispatch({
                type: DELETE_USER_FAIL
            });
        })
}

export const updateUser = (_id, user) => (dispatch) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify(user);

    axios.put(`${url_host || url_local}/api/user/update/${_id}`, body, config)
        .then(res => {
            dispatch({
                type: UPDATE_USER,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'UPDATE_USER_FAIL'))
            dispatch({
                type: UPDATE_USER_FAIL
            })
        })
}

export const getUser = (_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.get(`${url_host || url_local}/api/user/${_id}`)
            .then(res => {
                dispatch({
                    type: GET_USER,
                    payload: res.data
                })
                resolve()
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'GET_USER_FAIL'))
                dispatch({
                    type: GET_USER_FAIL
                })
                reject()
            })
    })
}