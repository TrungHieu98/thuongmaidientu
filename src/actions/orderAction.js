import axios from 'axios'

import { CREATE_ORDER, CREATE_ORDER_FAIL, GET_ORDERS, DELETE_ORDER, GET_ORDERS_FAIL, GET_ORDERS_HISTORY, GET_ORDERS_HISTORY_FAIL, GET_ORDER_DETAILS_FROM_ORDER, GET_ORDER_DETAILS_FROM_ORDER_FAIL } from '../constants'
const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const createOrder = (order) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    console.log('order', order)

    const body = JSON.stringify(order)

    axios.post(`${url_host || url_local}/api/order/create/`, body, config)
        .then(res => {
            dispatch({
                type: CREATE_ORDER,
            })
        })
        .catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status, 'CREATE_ORDER_FAIL'))
            dispatch({
                type: CREATE_ORDER_FAIL
            });
        })
}

export const getOrders = () => dispatch => {
    return new Promise((resolve, reject) => {
        axios.get(`${url_host || url_local}/api/order`)
            .then(res => {
                dispatch({
                    type: GET_ORDERS,
                    payload: res.data
                })
                resolve()
            })
            .catch(err => {
                console.log(err)
                dispatch({
                    type: GET_ORDERS_FAIL
                })
                reject()
            })
    })
}

export const deleteOrder = (order_id) => dispatch => {
    return new Promise((resolve, reject) => {
        axios.put(`${url_host || url_local}/api/order/delete/` + order_id)
            .then(res => {
                dispatch({
                    type: DELETE_ORDER
                })
                resolve()
            })
            .catch(err => {
                // dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_ORDER_FAIL'))
                // dispatch({
                //     type: DELETE_ORDER_FAIL
                // })
                reject()
            })
    })
}

export const getOrdersHistory = (user_id) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        user_id: user_id
    }
    const body = JSON.stringify(reqData)
    axios.post(`${url_host || url_local}/api/user/get/orders`, body, config)
        .then(res => {
            dispatch({
                type: GET_ORDERS_HISTORY,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err)
            dispatch({
                type: GET_ORDERS_HISTORY_FAIL,
            })
        })
}

export const getOrderDetails = (order_id) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        order_id: order_id
    }
    const body = JSON.stringify(reqData)
    axios.post(`${url_host || url_local}/api/order/getorderdetails`, body, config)
        .then(res => {
            dispatch({
                type: GET_ORDER_DETAILS_FROM_ORDER,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err)
            dispatch({
                type: GET_ORDER_DETAILS_FROM_ORDER_FAIL,
            })
        })
}