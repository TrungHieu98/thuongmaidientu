import {
    SET_UPDATING,
    SET_CREATING
} from '../constants';

export const setupUpdateForm = (_id) => (dispatch) => {
    dispatch({
        type: SET_UPDATING,
        payload: _id
    })
}

export const setupCreateForm = () => (dispatch) => {
    dispatch({
        type: SET_CREATING
    })
}