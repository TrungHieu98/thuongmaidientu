import axios from 'axios'
import {
    ADD_ADMIN_SUCCESS,
    ADD_ADMIN_FAIL,
    GET_ADMINS,
    GET_ADMINS_FAIL,
    DELETE_ADMIN,
    DELETE_ADMIN_FAIL,
    ADMINS_LOADING
} from '../constants'
import { returnErrors } from './errorAction';

const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const addAdmin = (email) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        email: email
    }
    const body = JSON.stringify(reqData);
    console.log('from addAdmin action: ', body)

    axios.post(`${url_host || url_local}/api/admin/add`, body, config)
        .then(res => dispatch({
            type: ADD_ADMIN_SUCCESS,
            payload: res.data
        }))
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'ADD_ADMIN_FAIL'))
            dispatch({
                type: ADD_ADMIN_FAIL
            })
        })
}

export const getAdmins = () => dispatch => {

    dispatch(setAdminLoading());

    axios.get(`${url_host || url_local}/api/admin`)
        .then(res => {
            dispatch({
                type: GET_ADMINS,
                payload: res.data
            });
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'GET_ADMINS_FAIL'))
            dispatch({
                type: GET_ADMINS_FAIL
            })
        })
}

export const setAdminLoading = () => {
    return {
        type: ADMINS_LOADING
    }
}

export const deleteAdmin = (admin_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.post(`${url_host || url_local}/api/admin/delete/${admin_id}`)
            .then(res => {
                dispatch({
                    type: DELETE_ADMIN
                })
                resolve()
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_ADMIN_FAIL'))
                dispatch({
                    type: DELETE_ADMIN_FAIL
                })
                reject(err)
            })
    })
}