import axios from 'axios'
import {
    GET_ALL_TYPE,
    GET_TYPES_FAIL,
    GET_TYPE,
    GET_TYPE_FAIL,
    DELETE_TYPE,
    DELETE_TYPE_FAIL,
    UPDATE_TYPE,
    UPDATE_TYPE_FAIL,
    CREATE_TYPE,
    CREATE_TYPE_FAIL,
    TYPES_LOADING,
    GET_TYPES_EXISTED,
    GET_TYPES_EXISTED_FAIL
} from '../constants'
import { returnErrors } from './errorAction'
const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

export const getAllProductType = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch(setTypeLoading())
        axios.get(`${url_host || url_local}/api/producttype`)
            .then(res => {
                dispatch({
                    type: GET_ALL_TYPE,
                    payload: res.data
                });
                resolve()
            })
            .catch(err => {
                //spatch(returnErrors(err.response.data, err.response.status, 'GET_DRIVERS_FAIL'));
                // dispatch({
                //     type: GET_ORDERS_FAIL
                // });
                reject()
            })
    })
}

export const getProductTypeExisted = () => dispatch => {
    axios.post(`${url_host || url_local}/api/producttype/getproducttypes`)
        .then(res => {
            dispatch({
                type: GET_TYPES_EXISTED,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch({
                type: GET_TYPES_EXISTED_FAIL
            })
        })
}

export const setTypeLoading = () => {
    return {
        type: TYPES_LOADING
    }
}

export const deleteType = (producttype_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.put(`${url_host || url_local}/api/producttype/delete/${producttype_id}`)
            .then(res => {
                dispatch({
                    type: DELETE_TYPE
                })
                resolve()
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_TYPE_FAIL'))
                dispatch({
                    type: DELETE_TYPE_FAIL
                });
                reject(err)
            })
    })
}

export const createType = (producttype) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        console.log(producttype)
        const body = JSON.stringify(producttype);
        console.log(body)
        axios.post(`${url_host || url_local}/api/producttype/create`, body, config)
            .then(res => {
                // console.log(res.data)
                dispatch({
                    type: CREATE_TYPE,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'CREATE_TYPE_FAIL'))
                dispatch({
                    type: CREATE_TYPE_FAIL
                })
                reject(err)
            })
    })
}

export const getType = (_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.get(`${url_host || url_local}/api/producttype/${_id}`)
            .then(res => {
                console.log('from productTypeAction: ', res.data)

                dispatch({
                    type: GET_TYPE,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'GET_TYPE_FAIL'))
                dispatch({
                    type: GET_TYPE_FAIL
                })
                reject(err)
            })
    })
}

export const updateType = (_id, producttype) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const body = JSON.stringify(producttype);

        axios.put(`${url_host || url_local}/api/producttype/update/${_id}`, body, config)
            .then(res => {
                dispatch({
                    type: UPDATE_TYPE,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'UPDATE_TYPE_FAIL'))
                dispatch({
                    type: UPDATE_TYPE_FAIL
                })
                reject(err)
            })
    })
}