import axios from 'axios'
import {
    ADD_ITEM, DELETE_ITEM,
    FETCH_ORDER_DETAILS,
    CART_LOADING,
    FETCH_ORDER_DETAILS_FAIL,
    CART_CONFIRMED,
    SET_PROVISONAL_SUM,
    SET_QUANTITY,
    CLEAR_LOCAL_STORAGE
} from '../constants';
import { returnErrors } from './errorAction'

const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;

const findAndSetQuantity = (product_id, items) => {
    return new Promise((resolve, reject) => {
        let duplicated = false
        let promises = []

        items.forEach(item => {
            let p = new Promise(resolve => {
                if (item._id === product_id) {
                    duplicated = true
                    item.quantity++
                }
                resolve(item)
            })

            promises.push(p)
        })

        Promise.all(promises)
            .then(result => {
                if (duplicated) {
                    console.log('duplicated', result)
                    resolve(items)
                }
                else {
                    const newItem = {
                        _id: product_id,
                        quantity: 1
                    }
                    console.log('not dup', [newItem, ...items])
                    resolve([newItem, ...items])
                }
            })
            .catch(err => reject(err))
    })
}

export const setQuantity = (index, value) => (dispatch) => {
    let items = JSON.parse(localStorage.getItem('items'))
    items[index].quantity += value
    if (items[index].quantity < 1)
        items[index].quantity = 1

    dispatch({
        type: SET_QUANTITY,
        payload: items
    })
}

export const addToCart = (product_id) => (dispatch) => {
    let items = []
    if (localStorage.getItem('items') !== null) {
        items = [...JSON.parse(localStorage.getItem('items'))]
        //kiem tra product_id co ton tai trong items hay ko
        //sau do them product_id va quantity = 1 neu chua ton tai, nguoc lai tang quantity cua item do them 1
        findAndSetQuantity(product_id, items)
            .then(newItems => {
                items = [...newItems]
                dispatch({
                    type: ADD_ITEM,
                    payload: items
                })
                alert('Đã thêm vào giỏ hàng')
            })
    }
    else {
        const newItem = {
            _id: product_id,
            quantity: 1
        }
        items = [newItem]
        dispatch({
            type: ADD_ITEM,
            payload: items
        })
        alert('Đã thêm vào giỏ hàng')
    }
}


export const deleteItem = (key) => dispatch => {
    return new Promise((resolve) => {
        const items = JSON.parse(localStorage.getItem('items'))
        items.splice(key, 1)
        dispatch({
            type: DELETE_ITEM,
            payload: items
        })
        resolve()
    })
}

export const fetchOrderDetail = (items) => dispatch => {
    return new Promise((resolve, reject) => {
        dispatch(setCartIsLoading())
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const myItems = {
            items: JSON.parse(items)
        }
        const body = JSON.stringify(myItems)
        axios.post(`${url_host || url_local}/api/cart/getorderdetail`, body, config)
            .then(res => {
                dispatch({
                    type: FETCH_ORDER_DETAILS,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                // dispatch(returnErrors(err.response.data, err.response.status, 'FETCH_ORDER_DETAIL_FAIL'))

                console.log('from cartAction', err)
                dispatch({
                    type: FETCH_ORDER_DETAILS_FAIL,
                })
                reject(err)
            })
    })
}

export const setCartIsLoading = () => dispatch => {
    dispatch({
        type: CART_LOADING,
    })
}

export const setCartConfirmed = () => dispatch => {
    dispatch({
        type: CART_CONFIRMED
    })
}

export const setProvisionalSum = (provisionalSum) => dispatch => {
    dispatch({
        type: SET_PROVISONAL_SUM,
        payload: provisionalSum
    })
}

export const removeItemsInLocalStorage = () => dispatch => {
    localStorage.removeItem('items')
    dispatch({
        type: CLEAR_LOCAL_STORAGE
    })
}