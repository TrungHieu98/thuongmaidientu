/* eslint-disable no-undef */
import axios from 'axios';
import {
    GET_PROMO,
    GET_PROMOS_BY_ADMIN,
    GET_PROMOS_BY_ADMIN_FAIL,
    GET_PROMO_FAIL,
    PROMOS_LOADING,
    DELETE_PROMO,
    DELETE_PROMO_FAIL,
    CREATE_PROMO_FAIL,
    CREATE_PROMO,
    UPDATE_PROMO,
    UPDATE_PROMO_FAIL,
    GET_PROMOS_OF_USER,
    GET_PROMOS_OF_USER_FAIL,
    SET_PROMO_SELECTED,
    TAKE_PROMO,
    TAKE_PROMO_FAIL,
    GET_PROMOS_BY_USER_FAIL,
    GET_PROMOS_BY_USER
} from '../constants';
import { returnErrors } from './errorAction';
const url_host = process.env.REACT_APP_HOST_URL;
const url_local = process.env.REACT_APP_LOCAL_URL;


export const getPromosByAdmin = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch(setPromoLoading());
        axios.get(`${url_host || url_local}/api/promo`)
            .then(res => {
                dispatch({
                    type: GET_PROMOS_BY_ADMIN,
                    payload: res.data
                })
                resolve()
            })
            .catch(err => {
                //spatch(returnErrors(err.response.data, err.response.status, 'GET_DRIVERS_FAIL'));
                // dispatch({
                //     type: GET_ORDERS_FAIL
                // });
                reject()
            })
    })
}

export const getPromoByUser = () => dispatch => {
    dispatch(setPromoLoading());
    axios.get(`${url_host || url_local}/api/promo/get/list`)
        .then(res => {
            console.log('res.data',res.data)
            dispatch({
                type: GET_PROMOS_BY_USER,
                payload: res.data
            })
        })
        .catch(err => {
            console.log('err',err)
            dispatch({
                type: GET_PROMOS_BY_USER_FAIL,
            })
        })
}

export const setPromoLoading = () => {
    return {
        type: PROMOS_LOADING
    }
}
export const deletePromo = (promo_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.put(`${url_host || url_local}/api/promo/delete/${promo_id}`)
            .then(res => {
                dispatch({
                    type: DELETE_PROMO
                })
                resolve()
            })
            .catch(err => {
                // dispatch(returnErrors(err.response.data, err.response.status, 'DELETE_PROMO_FAIL'))
                dispatch({
                    type: DELETE_PROMO_FAIL
                })
                reject()
            })
    })
}

export const createPromo = (promo) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const body = JSON.stringify(promo);

        axios.post(`${url_host || url_local}/api/promo/create`, body, config)
            .then(res => {
                dispatch({
                    type: CREATE_PROMO,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'CREATE_PROMO_FAIL'))
                dispatch({
                    type: CREATE_PROMO_FAIL
                })
                reject(err)
            })
    })
}

export const getPromo = (_id) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.get(`${url_host || url_local}/api/promo/${_id}`)
            .then(res => {
                console.log('from promoAction: ', res.data)

                dispatch({
                    type: GET_PROMO,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'GET_PROMO_FAIL'))
                dispatch({
                    type: GET_PROMO_FAIL
                })
                reject(err)
            })
    })
}

export const updatePromo = (_id, promo) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const body = JSON.stringify(promo);

        axios.put(`${url_host || url_local}/api/promo/update/${_id}`, body, config)
            .then(res => {
                dispatch({
                    type: UPDATE_PROMO,
                    payload: res.data
                })
                resolve(res.data)
            })
            .catch(err => {
                dispatch(returnErrors(err.response.data, err.response.status, 'UPDATE_PROMO_FAIL'))
                dispatch({
                    type: UPDATE_PROMO_FAIL
                })
                reject(err)
            })
    })
}

//promos la mang gom cac promo_id => tra ve mang gom cac object promo 
export const getPromosOfUser = (promos) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        promos: promos
    }
    const body = JSON.stringify(reqData);
    axios.post(`${url_host || url_local}/api/promo/getpromos`, body, config)
        .then(res => {
            dispatch({
                type: GET_PROMOS_OF_USER,
                payload: res.data
            })
        })
        .catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status, 'GET_PROMOS_OF_USER_FAIL'))
            dispatch({
                type: GET_PROMOS_OF_USER_FAIL
            })
        })
}

export const setPromoSelected = (promoSelected) => dispatch => {
    dispatch({
        type: SET_PROMO_SELECTED,
        payload: promoSelected
    })
}

export const takePromo = (user_id, promo_id) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const reqData = {
        user_id: user_id,
        promo_id: promo_id
    }
    const body = JSON.stringify(reqData)
    axios.post(`${url_host || url_local}/api/promo/takepromo`, body, config)
        .then(res => {
            dispatch({
                type: TAKE_PROMO
            })
            alert('Bạn đã nhận được mã khuyến mãi!')
        })
        .catch(err => {
            dispatch({
                type: TAKE_PROMO_FAIL
            })
            alert(err.response.data.msg)
        })
}