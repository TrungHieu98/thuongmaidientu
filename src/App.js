import React, { Component } from 'react'
import NavigationBar from "./components/navbar"
import Content from './components/content'
import Banner from './components/banner'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Provider } from 'react-redux'
import store from './Store'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { loadUser } from './actions/authAction'
// import { Card, Row, Col } from 'react-bootstrap'
import { Route } from 'react-router-dom'
import Footer from './components/footer'
import Grid from '@material-ui/core/Grid'
import { Container } from '@material-ui/core'
import SideBar from './components/sidebar'


class App extends Component {

    componentDidMount() {
        store.dispatch(loadUser());
    }

    render() {
        return (
            <Provider store={store}>
                <div className="App" style={{ backgroundColor: '#F5F6F6' }}>
                    <NavigationBar />
                    <Route exact path="/home" component={Banner} />
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        spacing={2}
                        style={{marginTop: '2rem'}}
                    >
                        <Grid item >
                            <SideBar />
                        </Grid>
                        <Grid item>
                            <Content />
                        </Grid>
                    </Grid>
                    <Footer />
                </div>
            </Provider >
        )
    }
}

export default App