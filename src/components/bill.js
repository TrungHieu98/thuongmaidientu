import React, { Component } from 'react'
import { Form, Container, Row, Col, Alert, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import CartList from './lists/cartList'
import { fetchOrderDetail } from '../actions/cartAction'
import PaypalBtn from './paypal'
import bson from 'bson-objectid'
import PromoSelection from './promoSelection'
import { Typography, CardContent } from '@material-ui/core'
import { createOrder } from '../actions/orderAction'
import { createOrderDetails } from '../actions/orderDetailAction'
import { removePromoUsed, recordOrderOfUser } from '../actions/authAction'
import Card from '@material-ui/core/Card';

class Bill extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: '',
            phone: '',
            orderDate: '',
            shipDate: '',
            receiverAddress: '',
            note: '',
            msg: null,
            isFull: false,
        }
    }

    // static propTypes = {
    //     items: PropTypes.string.isRequired,
    //     fetchOrderDetail: PropTypes.func.isRequired
    // }

    componentDidMount() {
        if (this.props.user !== null) {
            const { userName, phone, address } = this.props.user

            this.setState({
                userName: userName,
                phone: phone,
                receiverAddress: address,
                orderDate: this.getCurrentDate(),
                shipDate: this.getDayLater(3)
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.user !== this.props.user && this.props.user !== null) {
            const { userName, phone, receiverAddress } = this.props.user

            this.setState({
                userName: userName,
                phone: phone,
                receiverAddress: receiverAddress
            })
        }
    }

    changeHandler(e) {
        this.setState({
            [e.target.name]: e.target.value
        })

        const { user, orderDate, shipDate, receiverAddress, note } = this.state

        if (user !== null &&
            orderDate !== '' &&
            shipDate !== '' &&
            receiverAddress !== '' &&
            note !== '' &&
            this.props.orderDetails !== []) {

            this.setState({ isFull: true })
        }
        else {
            this.setState({ isFull: false })
        }
    }

    async createOrderHandler() {
        const order_id = bson().str
        const { user, orderDetailsInCart, promoSelected, provisionalSum, total, discount } = this.props
        const { orderDate, shipDate, receiverAddress, note } = this.state
        let promoUsed = null
        if (promoSelected)
            promoUsed = promoSelected._id

        const orderDetails = await this.props.createOrderDetails(orderDetailsInCart, order_id)
        const newOrder = {
            _id: order_id,
            user: user,
            orderDate: orderDate,
            shipDate: shipDate,
            receiverAddress: receiverAddress,
            note: note,
            orderDetails: orderDetails,
            promoUsed: promoUsed,
            provisionalSum: provisionalSum,
            total: total,
            discount: discount
        }
        console.log('newOrder', newOrder)
        this.props.createOrder(newOrder)
        this.props.recordOrderOfUser(order_id, user._id)
    }

    getCurrentDate = () => {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        return today = yyyy + '-' + mm + '-' + dd;
    }

    getDayLater = (numberDay) => {
        const today = new Date()
        var later = new Date(today)
        later.setDate(later.getDate() + numberDay)

        var dd = String(later.getDate()).padStart(2, '0');
        var mm = String(later.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = later.getFullYear();

        return later = yyyy + '-' + mm + '-' + dd;
    }

    checkAtLeastOneOrderDetail = () => {
        if (this.props.orderDetailsInCart.length > 0)
            return true
        else {
            alert('Hãy chọn ít nhất một sản phẩm để thanh toán')
            return false
        }
    }

    render() {
        const { orderDate, shipDate, receiverAddress, note } = this.state
        const { isAuthenticated } = this.props

        return (
            <div>
                {
                    isAuthenticated && this.checkAtLeastOneOrderDetail() ?
                        <Container>
                            <Row>
                                <Col>
                                    <Card>
                                        <CardContent>
                                            <Form>
                                                <Form.Label className="font-weight-bold">THÔNG TIN ĐƠN HÀNG</Form.Label>

                                                {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Tên khách hàng:</Form.Label>
                                                    <Form.Control type="text" value={this.state.userName} name="userName" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Số điện thoại:</Form.Label>
                                                    <Form.Control type="number" value={this.state.phone} name="phone" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Ngày đặt:</Form.Label>
                                                    <Form.Control type="date" readOnly value={this.state.orderDate} name="orderDate" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Ngày giao:</Form.Label>
                                                    <Form.Control type="date" readOnly value={this.state.shipDate} name="shipDate" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Địa chỉ nhận:</Form.Label>
                                                    <Form.Control type="text" value={this.state.receiverAddress} name="receiverAddress" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>

                                                <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                                    <Form.Label style={{ float: 'left' }}>Ghi chú:</Form.Label>
                                                    <Form.Control type="text" value={this.state.note} name="note" onChange={(e) => this.changeHandler(e)} />
                                                </Form.Group>
                                            </Form>
                                        </CardContent>
                                    </Card>

                                </Col>

                                <Col>
                                    <Row>
                                        <CartList />
                                    </Row>
                                    <Row>
                                        <Col style={{ marginTop: '2rem' }}>
                                            <PromoSelection />
                                        </Col>
                                        <Col style={{ marginTop: '2rem' }}>
                                            <Row>
                                                <Typography>
                                                    Giá trị giảm: -{this.props.discount}
                                                </Typography>
                                            </Row>
                                            <Row>
                                                <Typography>
                                                    <strong>
                                                        Tổng đơn hàng: {this.props.total}
                                                    </strong>
                                                </Typography>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <div style={{ marginTop: '4rem' }}>
                                            {this.state.isFull ?
                                                <PaypalBtn
                                                    orderDate={orderDate}
                                                    shipDate={shipDate}
                                                    receiverAddress={receiverAddress}
                                                    note={note}
                                                />
                                                : <Alert style={{ marginTop: '4rem' }} variant="danger">Vui lòng nhập đủ thông tin để thanh toán</Alert>}
                                        </div>
                                    </Row>

                                    {/* <Button onClick={() => this.setState({ flag: true })}>Go Home</Button> */}
                                    {/* <div>
                                        <Button onClick={() => { this.props.fetchOrderDetail(this.props.items) }}>reload order</Button>
                                    </div> */}

                                </Col>
                            </Row>
                        </Container>
                        :
                        <Redirect to='/home' />
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    items: state.cart.items,
    orderDetailsInCart: state.cart.orderDetailsInCart,
    isAuthenticated: state.auth.isAuthenticated,
    // isAuthenticated: true,
    promoSelected: state.promo.promoSelected,
    user: state.auth.user,
    provisionalSum: state.cart.provisionalSum,
    discount: state.bill.discount,
    total: state.bill.total
})

const mapDispatchToProps = {
    createOrder,
    createOrderDetails,
    removePromoUsed,
    recordOrderOfUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Bill);