import React, { Component } from "react"
import { Modal } from 'react-bootstrap'
import { clearErrors } from '../actions/errorAction'
import { connect } from 'react-redux'
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined'
import CartList from './lists/cartList'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
// import Fab from '@material-ui/core/Fab';
// import AddIcon from '@material-ui/icons/Delete';


class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }

    handleConfirm = () => {
        if (this.props.isAuthenticated) {
            this.handleClose()
        }
        else
            alert('Bạn cần đăng nhập để xác nhận mua hàng')
    }

    handleClose = () => {
        this.props.clearErrors();
        this.setState({ show: false });
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    render() {
        return (
            <div>
                <Button
                    variant="contained"
                    size="medium"
                    style={{ backgroundColor: '#ffab00' }}
                    aria-label="cart"
                    onClick={() => this.handleShow()}>
                    <ShoppingCartOutlinedIcon />
                </Button>
                <Modal show={this.state.show} onHide={() => this.handleClose()}>
                    <Modal.Header closeButton>
                        <Modal.Title>Giỏ hàng</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CartList />
                    </Modal.Body>
                    <Link style={{ marginLeft: '4rem', marginBottom: '2rem' }} onClick={() => { this.handleClose() }} to="/home/bill">
                        <Button variant="contained" color="primary" size='large' onClick={this.handleConfirm}>Xác nhận</Button>
                    </Link>
                </Modal>
            </div >
        )
    }
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    items: state.cart.items,
    orderDetailsInCart: state.cart.orderDetailsInCart,
})

export default connect(mapStateToProps, { clearErrors })(Cart);