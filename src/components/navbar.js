import React, { Component, Fragment } from 'react'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import RegisterModal from './registerModal'
import Logout from './logout'
import Login from './login'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Cart from './cart'
import Button from '@material-ui/core/Button'
//import { clearErrors } from '../actions/errorAction'

// import PropTypes from 'prop-types'

class NavigationBar extends Component {

    render() {
        const guestLinks = (
            <Fragment>
                <Nav.Item>
                    <RegisterModal />
                </Nav.Item>

                <Nav.Item>
                    <Login />
                </Nav.Item>
            </Fragment>
        )
        const authLinks = (
            <Fragment>
                {/* <Nav.Link style={{ color: 'white' }}>
                    <span className="mr-3">
                        <strong>{this.props.user ? `Chào ${this.props.user.userName}` : ''}</strong>
                    </span>
                </Nav.Link> */}
                {/* <Nav.Item>
                    <UserMenu/>
                </Nav.Item> */}
                <Nav.Item>
                    <NavDropdown title={this.props.user ? `${this.props.user.userName}` : ''} id="basic-nav-dropdown" style={{ color: 'white' }}>
                        <NavDropdown.Item>
                            <Link to='/home/profile' style={{ textDecoration: 'none' }}>Thông tin tài khoản</Link>
                        </NavDropdown.Item>
                        <NavDropdown.Item>
                            <Link to='/home/history' style={{ textDecoration: 'none' }}>Lịch sử đơn hàng</Link>
                        </NavDropdown.Item>
                    </NavDropdown>
                </Nav.Item>

                <Nav.Item>
                    <Logout />
                </Nav.Item>
            </Fragment>
        )

        const dropdownAdmin = (
            <Fragment>

                <NavDropdown title="ADMIN" id="basic-nav-dropdown" style={{ color: 'white' }}>
                    <NavDropdown.Item>
                        <Link to="/admin/product" style={{ textDecoration: 'none' }}>Sản phẩm</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                        <Link to="/admin/promo" style={{ textDecoration: 'none' }}>Khuyến mãi</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                        <Link to="/admin/producttype" style={{ textDecoration: 'none' }}>Loại sản phẩm</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                        <Link to="/admin/order" style={{ textDecoration: 'none' }}>Đơn hàng</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Divider />

                    <NavDropdown.Item>
                        <Link to="/admin/user" style={{ textDecoration: 'none' }}>Người dùng</Link>
                    </NavDropdown.Item>

                    <NavDropdown.Item>
                        <Link to="/admin/manager" style={{ textDecoration: 'none' }}>Admin</Link>
                    </NavDropdown.Item>

                </NavDropdown>
            </Fragment>
        )

        return (
            <div>
                <Navbar className="navbar navbar-dark" style={{ backgroundColor: '#202932' }} expand="lg">
                    <Link style={{ textDecoration: 'none' }} to="/home">
                        <Navbar.Brand>
                            <img
                                alt=""
                                src=""
                                className="d-inline-block align-top"
                            />
                            {'FAHASA'}
                        </Navbar.Brand>
                    </Link>

                    <Navbar.Toggle aria-controls="basic-navbar-nav" />

                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav.Link>
                            <Link style={{ textDecoration: 'none' }} to="/home">
                                <Button variant="text" color="primary" size='small' style={{ color: 'white', textDecoration: 'none', border: 'none', outline: 'none' }}>Trang chủ</Button>
                            </Link>
                        </Nav.Link>

                        <Nav.Link>
                            <Link style={{ textDecoration: 'none' }} to="/promo">
                                <Button variant="text" color="primary" size='small' style={{ color: 'white', textDecoration: 'none', border: 'none', outline: 'none' }}>Khuyến mãi</Button>
                            </Link>
                        </Nav.Link>

                        <Nav className="ml-auto">
                            <Nav style={{ marginRight: '1rem' }}>
                                <Cart />
                            </Nav>
                            <Nav className="ml-auto" >
                                {this.props.isAdmin ? dropdownAdmin : null}
                            </Nav>

                            {this.props.isAuthenticated ? authLinks : guestLinks}
                        </Nav>

                    </Navbar.Collapse>
                </Navbar >
            </div >
        )
    }
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    isAdmin: state.auth.isAdmin,
    user: state.auth.user
})
export default connect(mapStateToProps, {})(NavigationBar)