import React, { Component } from 'react'
import { Form, Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import { clearErrors } from '../../actions/errorAction'
import { updateType, createType, getAllProductType, getType } from '../../actions/productTypeAction'
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

//cach import:
//để gọi form update -> <ProductTypeModal task='update' _id={_id}/>
//để gọi form add -> <ProductTypeModal task='add'/>

class ProductTypeModal extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            typeName: '',
            show: false,
        }
    }

    handleClose = () => {
        this.props.clearErrors()
        this.setState({ show: false })
    }

    handleShow() {
        this.setState({ show: true })

        if (this.props.task === 'update')
            this.setValueForInput()
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { typeName } = this.state
        const { _id, task, updateType, createType, getAllProductType } = this.props

        const data = {
            typeName: typeName,
        }
        if (task === 'update') {
            updateType(_id, data)
        }
        else if (task === 'add') {
            createType(data)
        }
        getAllProductType()
        this.handleClose()
    }

    setValueForInput() {
        this.props.getType(this.props._id)
            .then(type => {
                this.setState({
                    typeName: type.typeName,
                })
            })
    }

    // componentDidUpdate(prevProps) {
    //     const { error } = this.props;
    //     if (error !== prevProps.error) {
    //         if (error.id === 'UPDATE_USER_FAIL') {
    //             this.setState({ msg: error.msg.msg });
    //         } else {
    //             this.setState({ msg: null });
    //         }
    //     }
    // }

    render() {
        const addButton = (
            <Button variant="contained" color="primary" type="submit">Xác nhận</Button>
        )

        const updateButton = (
            <Button variant="contained" color="primary" type="submit">Cập nhật</Button>

        )

        return (
            <div>
                {this.props.task === 'add' ?
                    <Button onClick={() => this.handleShow()} variant="contained" color='primary' type="submit" size='large'>THÊM LOẠI SẢN PHẨM</Button>
                    :
                    <IconButton onClick={() => this.handleShow()} variant="contained" color="primary" type="submit">
                        <EditIcon />
                    </IconButton>
                }

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>THÔNG TIN LOẠI SẢN PHẨM</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form onSubmit={this.handleSubmit} style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Loại sản phẩm:</Form.Label>
                                <Form.Control type="text" name="typeName" value={this.state.typeName} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            {this.props.task === 'add' ? addButton : updateButton}
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    // productTypes: state.productType.productTypes,
    error: state.error
})

const mapDispatchToProps = {
    createType,
    getType,
    updateType,
    clearErrors,
    getAllProductType,
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductTypeModal);