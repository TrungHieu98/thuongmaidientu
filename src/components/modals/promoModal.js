import React, { Component } from 'react'
import { Form, Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import { clearErrors } from '../../actions/errorAction'
import { updatePromo, createPromo, getPromosByAdmin, getPromo } from '../../actions/promoAction'
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

//cach import:
//để gọi form update -> <ProductTypeModal task='update' _id={_id}/>
//để gọi form add -> <ProductTypeModal task='add'/>

class PromoModal extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            promoName: '',
            percent: '',
            maxDiscount: '',
            minOrderPrice: '',
            description: '',
            quantity: '',
            producer: '',
            expiredDate: null,
            urlImage: '',
            show: false,
        }
    }

    handleClose = () => {
        this.props.clearErrors()
        this.setState({ show: false })
    }

    handleShow() {
        this.setState({ show: true })

        if (this.props.task === 'update')
            this.setValueForInput()
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {
            promoName,
            percent,
            maxDiscount,
            minOrderPrice,
            description,
            quantity,
            producer,
            expiredDate,
            urlImage } = this.state

        const data = {
            promoName: promoName,
            percent: percent,
            maxDiscount: maxDiscount,
            minOrderPrice: minOrderPrice,
            description: description,
            quantity: quantity,
            producer: producer,
            expiredDate: expiredDate,
            urlImage: urlImage
        }

        const { _id, task, updatePromo, createPromo } = this.props
        if (task === 'update')
            updatePromo(_id, data)

        else if (task === 'add')
            createPromo(data)

        this.props.getPromosByAdmin()
        this.handleClose()
    }

    setValueForInput() {
        this.props.getPromo(this.props._id)
            .then(promo => {
                this.setState({
                    promoName: promo.promoName,
                    percent: promo.percent,
                    maxDiscount: promo.maxDiscount,
                    minOrderPrice: promo.minOrderPrice,
                    description: promo.description,
                    quantity: promo.quantity,
                    producer: promo.producer,
                    expiredDate: promo.expiredDate,
                    urlImage: promo.urlImage
                })
            })
    }

    // componentDidUpdate(prevProps) {
    //     const { error } = this.props;
    //     if (error !== prevProps.error) {
    //         if (error.id === 'UPDATE_USER_FAIL') {
    //             this.setState({ msg: error.msg.msg });
    //         } else {
    //             this.setState({ msg: null });
    //         }
    //     }
    // }

    render() {
        const addButton = (
            <Button variant="contained" color="primary" type="submit">Xác nhận</Button>
        )

        const updateButton = (
            <Button variant="contained" color="primary" type="submit">Cập nhật</Button>
        )

        return (
            <div>
                {this.props.task === 'add' ?
                    <Button onClick={() => this.handleShow()} variant="contained" color="primary" type="submit" size='large'>THÊM KHUYẾN MÃI</Button>
                    :
                    <IconButton onClick={() => this.handleShow()} variant="contained" color="primary" type="submit">
                        <EditIcon />
                    </IconButton>
                }

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>THÔNG TIN KHUYẾN MÃI</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form onSubmit={this.handleSubmit} style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Tên khuyến mãi:</Form.Label>
                                <Form.Control type="text" name="promoName" value={this.state.promoName} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Phần trăm khuyến mãi:</Form.Label>
                                <Form.Control type="text" name="percent" value={this.state.percent} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Mô tả:</Form.Label>
                                <Form.Control type="text" name="description" value={this.state.description} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Giá trị giảm tối đa:</Form.Label>
                                <Form.Control type="text" name="maxDiscount" value={this.state.maxDiscount} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Giá trị đơn hàng tối thiếu:</Form.Label>
                                <Form.Control type="text" name="minOrderPrice" value={this.state.minOrderPrice} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Số lượng:</Form.Label>
                                <Form.Control type="number" name="quantity" value={this.state.quantity} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Ngày hết hạn:</Form.Label>
                                <Form.Control type="date" name="expiredDate" value={this.state.expiredDate} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Nhà cung cấp:</Form.Label>
                                <Form.Control type="text" name="producer" value={this.state.producer} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>URL ảnh:</Form.Label>
                                <Form.Control type="text" name="urlImage" value={this.state.urlImage} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            {this.props.task === 'add' ? addButton : updateButton}
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    error: state.error
})

const mapDispatchToProps = {
    createPromo,
    getPromo,
    updatePromo,
    clearErrors,
    getPromosByAdmin,
}

export default connect(mapStateToProps, mapDispatchToProps)(PromoModal);