import React, { Component, Alert } from 'react'
import { Form, Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import { clearErrors } from '../../actions/errorAction'
import { createProduct, updateProduct, getProduct, getProductsByAdmin } from '../../actions/productAction'
import { getAllProductType, getProductTypeExisted } from '../../actions/productTypeAction'

import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

//cach import:
//để gọi form update -> <ProductModal task='update' _id={_id}/>
//để gọi form add -> <ProductModal task='add'/>

class ProductModal extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            productName: '',
            price: '',
            urlImage: '',
            productType: '',
            show: false,
            msg: null,
            selection: ''
        }
    }

    handleClose = () => {
        this.props.clearErrors()
        this.setState({ show: false })
    }

    handleShow() {
        this.setState({ show: true })
        this.props.getProductTypeExisted()

        if (this.props.task === 'update')
            this.setValueForInput()
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { productName, price, urlImage, productType } = this.state
        const { _id, task, updateProduct, createProduct } = this.props

        const data = {
            productName: productName,
            price: price,
            urlImage: urlImage,
            productType: productType
        }
        if (task === 'update') {
            updateProduct(_id, data)
        }
        else if (task === 'add') {
            createProduct(data)
        }
        this.props.getProductsByAdmin()
        if (this.state.msg === null) this.handleClose()
    }

    setValueForInput() {
        this.props.getProduct(this.props._id)
            .then(product => {
                this.setState({
                    productName: product.productName,
                    price: product.price,
                    urlImage: product.urlImage
                })
            })
    }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            if (error.id === 'CREATE_PRODUCT_FAIL') {
                this.setState({ msg: error.msg.msg });
            } else {
                this.setState({ msg: null });
            }
        }
    }

    render() {
        const { productTypes } = this.props

        const addButton = (
            <Button variant="contained" color="primary" type="submit">Xác nhận</Button>
        )

        const updateButton = (
            <Button variant="contained" color="primary" type="submit">Cập nhật</Button>

        )

        return (
            <div>
                {this.props.task === 'add' ?
                    <Button onClick={() => this.handleShow()} variant="contained" color='primary' type="submit" size='large'>THÊM SẢN PHẨM</Button>
                    :
                    <IconButton onClick={() => this.handleShow()} variant="contained" color="primary" type="submit">
                        <EditIcon />
                    </IconButton>
                }

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>THÔNG TIN SẢN PHẨM</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form onSubmit={this.handleSubmit} style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                            {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Tên sản phẩm:</Form.Label>
                                <Form.Control type="text" name="productName" value={this.state.productName} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Giá:</Form.Label>
                                <Form.Control type="number" name="price" value={this.state.price} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Hình ảnh sản phẩm:</Form.Label>
                                <Form.Control type="text" name="urlImage" value={this.state.urlImage} onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Loại sản phẩm:</Form.Label>
                                <Form.Control name="productType" as="select" multiple onChange={(e) => this.changeHandler(e)}>
                                    {productTypes !== null ?
                                        productTypes.map(productType => {
                                            return (
                                                <option value={productType._id}>{productType.typeName}</option>
                                            )
                                        })
                                        : null}

                                </Form.Control>
                            </Form.Group>
                            {this.props.task === 'add' ? addButton : updateButton}
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    productTypes: state.productType.productTypes,
    error: state.error
})

const mapDispatchToProps = {
    createProduct,
    getProduct,
    updateProduct,
    clearErrors,
    getAllProductType,
    getProductsByAdmin,
    getProductTypeExisted
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductModal);