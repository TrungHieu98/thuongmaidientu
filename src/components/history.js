import React, { Component, Fragment } from 'react';
import { Row, Col, Container, Card } from 'react-bootstrap';
import { FadeLoader } from 'react-spinners';
import { getOrdersHistory } from '../actions/orderAction';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Material-UI
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button'
import MaterialTable, { MTableToolbar } from 'material-table';
import OrderDetail from './orderDetail'
import { Redirect } from 'react-router-dom';

class History extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Ngày đặt', field: 'orderDate' },
                { title: 'Ngày giao', field: 'shipDate' },
                { title: 'Địa chỉ nhận', field: 'receiverAddress' },
                { title: 'Ghi chú', field: 'note' },
                { title: 'Tạm tính', field: 'provisionalSum' },
                { title: 'Giảm giá', field: 'discount' },
                { title: 'Tổng đơn hàng', field: 'total' },
            ],
            data: [],
        }
    }

    // static propTypes = {
    //     user: PropTypes.object.isRequired,
    // }

    componentDidMount() {
        if (this.props.user !== null)
            this.props.getOrdersHistory(this.props.user._id)
    }

    componentDidUpdate(prevProps) {
        const { user, ordersHistory } = this.props
        if (ordersHistory !== prevProps.ordersHistory) {
            let arr = [];
            ordersHistory.forEach(order => {
                const temp = {
                    _id: order._id,
                    orderDate: order.orderDate,
                    shipDate: order.shipDate,
                    receiverAddress: order.receiverAddress,
                    note: order.note,
                    provisionalSum: '$' + order.provisionalSum,
                    total: '$' + order.total,
                    discount: '$' + order.discount
                }

                arr = [...arr, temp]
            })
            this.setState({
                data: arr
            })
        }

        if (user !== prevProps.user && user !== null)
            this.props.getOrdersHistory(user._id)
    }

    render() {
        const { data } = this.state;
        if (this.props.user !== null) {
            if (data.length > 0) {
                return (
                    <div>
                        <Grow in={true}>
                            <MaterialTable
                                title="LỊCH SỬ ĐƠN HÀNG"
                                columns={this.state.columns}
                                data={this.state.data}
                                actions={[
                                    {
                                        icon: 'save',
                                        tooltip: 'Save User',
                                    },
                                ]}
                                components={{
                                    Toolbar: props => (
                                        <div style={{ backgroundColor: '#e8eaf5' }}>
                                            <MTableToolbar {...props} />
                                        </div>
                                    ),
                                    Action: props => {
                                        return (
                                            <OrderDetail order_id={props.data._id} />
                                        )
                                    }
                                }}
                            />
                        </Grow>
                    </div>
                )
            }
            else {
                return (
                    <p>Bạn chưa có đơn hàng nào trước đây</p>
                    // <Container>
                    //     <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                    //         <Col xs lg="2"></Col>
                    //         <Col md="auto">
                    //             <div style={{ float: 'center' }}>
                    //                 <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                    //                     <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                    //                 </FadeLoader>
                    //             </div>

                    //         </Col>
                    //         <Col xs lg="2"></Col>
                    //     </Row>
                    // </Container>
                )
            }
        } else {
            return (
                <Redirect to='/home' />
            )
        }

    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    ordersHistory: state.order.ordersHistory
})

const mapDispatchToProps = ({
    getOrdersHistory
})

export default connect(mapStateToProps, mapDispatchToProps)(History)