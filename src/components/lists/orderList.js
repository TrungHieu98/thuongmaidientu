import React, { Component, Fragment } from 'react'
import { Row, Col, Container, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { FadeLoader } from 'react-spinners';
// action
import { getOrders, deleteOrder } from '../../actions/orderAction'
import { setupUpdateForm } from '../../actions/formAction'
import { connect } from 'react-redux'
import OrderForm from '../../components/forms/orderForm'
// Material-UI
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grow from '@material-ui/core/Grow';
import MaterialTable, { MTableToolbar } from 'material-table';

class OrderList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Tên khách hàng', field: 'userName' },
                { title: 'Ngày đặt', field: 'orderDate' },
                { title: 'Ngày giao', field: 'shipDate' },
                { title: 'Địa chỉ nhận', field: 'receiverAddress' },
                { title: 'Ghi chú', field: 'note' },
                { title: 'Trạng thái', field: 'isDeleted' }
            ],
            data: [],
        }
    }
    setupUpdateForm(e, _id) {
        e.preventDefault()

        this.props.setupUpdateForm(_id)
    }

    componentDidMount() {
        this.props.getOrders()
    }

    componentDidUpdate(prevProps) {
        const { orders } = this.props
        if (orders !== prevProps.orders) {
            let arr = [];
            orders.forEach(order => {
                let status
                if (order.isDeleted)
                    status = 'Đã xóa'
                else
                    status = 'Tồn tại'

                const temp = {
                    _id: order._id,
                    userName: order.user.userName,
                    orderDate: order.orderDate,
                    shipDate: order.shipDate,
                    receiverAddress: order.receiverAddress,
                    note: order.note,
                    isDeleted: status
                }

                arr = [...arr, temp]
            })
            this.setState({
                data: arr
            })
        }
    }

    handleDelete(e, _id) {
        e.preventDefault();
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            this.props.deleteOrder(_id)
                .then(
                    this.props.getOrders()
                )
        }
    }

    render() {
        const { data } = this.state;
        if (data.length > 0) {
            return (
                <Card>
                    <Grow in={true}>
                        <MaterialTable
                            title="DANH SÁCH ĐƠN HÀNG"
                            columns={this.state.columns}
                            data={this.state.data}
                            actions={[
                                {
                                    icon: 'save',
                                    tooltip: 'Save User',
                                    Delete: (e, _id) => this.handleDelete(e, _id),
                                },
                            ]}
                            components={{
                                Toolbar: props => (
                                    <div style={{ backgroundColor: '#e8eaf5' }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                                Action: props => {
                                    // console.log(props.data)
                                    return (
                                        <Row>
                                            {/* <PromoModal task='update' _id={props.data._id} /> */}

                                            {props.data.isDeleted === 'Đã xóa' ?
                                                null
                                                :
                                                <IconButton aria-label="delete" style={{ color: '#ec2F4B' }}
                                                    onClick={(e) => props.action.Delete(e, props.data._id)}
                                                // isDisable='true'
                                                >
                                                    <DeleteIcon />
                                                </IconButton>
                                            }
                                        </Row>
                                    )
                                }
                            }}
                        />
                    </Grow>
                </Card>
            )
        }
        else {
            return (
                <Container>
                    <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                        <Col xs lg="2"></Col>
                        <Col md="auto">
                            <div style={{ float: 'center' }}>
                                <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                                    <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                                </FadeLoader>
                            </div>

                        </Col>
                        <Col xs lg="2"></Col>
                    </Row>
                </Container>
            )
        }
    }
}

const mapStateToProps = state => ({
    orders: state.order.orders,
    isLoading: state.productType.isLoading,
    error: state.error
})

export default connect(mapStateToProps, { getOrders, deleteOrder, setupUpdateForm })(OrderList)
