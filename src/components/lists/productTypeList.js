import React, { Component } from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import { FadeLoader } from 'react-spinners'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import Grow from '@material-ui/core/Grow'
import MaterialTable, { MTableToolbar } from 'material-table'
import { getAllProductType, deleteType } from '../../actions/productTypeAction'
import { connect } from 'react-redux'
import ProductTypeModal from '../modals/productTypeModal'

// export class ProductTypeList extends Component {

//     setupUpdateForm(e, _id) {
//         e.preventDefault()

//         this.props.setupUpdateForm(_id)
//     }

//     componentDidMount() {
//         this.props.getAllProductType();
//     }

//     async handleDelete(e, _id) {
//         e.preventDefault();
//         // eslint-disable-next-line no-restricted-globals
//         if (confirm('Bạn thật sự muốn xóa?') === true) {
//             await this.props.deleteType(_id)
//             this.props.getAllProductType()
//         }
//     }
//     render() {
//         const { productTypes } = this.props;

//         const loadingComponent = (
//             <Fragment>
//                 <td colSpan='5' align='center'>
//                     <BeatLoader color="#50E3C2" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
//                         <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
//                     </BeatLoader>
//                 </td>
//             </Fragment>
//         )

//         const loadedComponent = (
//             <Fragment>
//                 {productTypes !== null ? productTypes.map(type => {
//                     const { _id,typeName } = type;

//                     return (
//                         <tr>
//                             <td>{typeName}</td>

//                             <td>
//                                 <ButtonGroup aria-label="Basic example">
//                                    <Button onClick={(e) => { this.setupUpdateForm(e, _id) }} variant="secondary">Sửa</Button>
//                                     <Button onClick={(e) => { this.handleDelete(e, _id) }} variant="danger">Xóa</Button>
//                                 </ButtonGroup>
//                             </td>

//                         </tr>
//                     )
//                 }):null}
//             </Fragment>
//         )

//         return (
//             <div className="shadow-lg">
//                 {/* <Button variant="outline-primary">Create</Button> */}
//                 <Table hover responsive >
//                     <thead>
//                         <tr>
//                             <th>Tên Loại sản phẩm</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {!this.props.loading ? loadedComponent : loadingComponent}
//                     </tbody>
//                 </Table>
//             </div>
//         )

//     }
// }

// const mapStateToProps = state => ({
//     productTypes: state.productType.productTypes,
//     loading: state.productType.loading,
//     error: state.error
// })

// export default connect(mapStateToProps, { getAllProductType, deleteType, setupUpdateForm })(ProductTypeList)

export class ProductTypeList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Tên loại sản phẩm', field: 'typeName' },
                { title: 'Tình trạng', field: 'isDeleted' }
            ],
            data: [],
        }
    }

    handleDelete(e, _id) {
        e.preventDefault();
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            this.props.deleteType(_id)
            this.props.getAllProductType()
        }
    }

    componentDidMount() {
        this.props.getAllProductType()
    }

    componentDidUpdate(prevProps) {
        const { productTypes } = this.props;
        if (productTypes !== prevProps.productTypes) {
            let arr = [];
            productTypes.forEach(productType => {
                let status
                if (productType.isDeleted)
                    status = 'Đã xóa'
                else
                    status = 'Tồn tại'

                const temp = {
                    _id: productType._id,
                    typeName: productType.typeName,
                    isDeleted: status
                }

                arr = [...arr, temp]
            });
            this.setState({
                data: arr
            })
        }
    }

    // async componentDidMount() {
    //     await this.props.getAllProductType()
    //         .then(() => {
    //             const { productTypes } = this.props;

    //             let newTypes = [];
    //             productTypes.forEach(type => {
    //                 const newType = {
    //                     typeName: type.typeName
    //                 }
    //                 newTypes = [...newTypes, newType]
    //             });
    //             this.setState({
    //                 data: newTypes
    //             })
    //         })
    // }

    render() {
        const { data } = this.state;
        if (data.length > 0) {
            return (
                <div>
                    <Grow in={true}>
                        <MaterialTable
                            title="DANH SÁCH LOẠI SẢN PHẨM"
                            columns={this.state.columns}
                            data={this.state.data}
                            actions={[
                                {
                                    icon: 'save',
                                    tooltip: 'Save User',
                                    Delete: (e, _id) => this.handleDelete(e, _id),
                                },

                            ]}
                            components={{
                                Toolbar: props => (
                                    <div style={{ backgroundColor: '#e8eaf5' }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                                Action: props => {
                                    return (
                                        <Row>
                                            <ProductTypeModal task='update' _id={props.data._id} />

                                            {props.data.isDeleted === 'Đã xóa' ?
                                                null
                                                :
                                                <IconButton aria-label="delete" style={{ color: '#ec2F4B' }}
                                                    onClick={(e) => props.action.Delete(e, props.data._id)}
                                                // isDisable='true'
                                                >
                                                    <DeleteIcon />
                                                </IconButton>
                                            }
                                        </Row>
                                    )
                                }
                            }}
                        />
                    </Grow>
                </div>
            )
        }
        else {
            return (
                <Container>
                    <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                        <Col xs lg="2"></Col>
                        <Col md="auto">
                            <div style={{ float: 'center' }}>
                                <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                                    <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                                </FadeLoader>
                            </div>

                        </Col>
                        <Col xs lg="2"></Col>
                    </Row>
                </Container>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    productTypes: state.productType.productTypes,
    loading: state.productType.loading,
    error: state.error
})

const mapDispatchToProps = {
    getAllProductType,
    deleteType
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductTypeList)