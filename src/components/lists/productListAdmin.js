import React, { Component, Fragment } from 'react';
import { Table, Button, ButtonGroup, Row, Col, Container } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import { FadeLoader } from 'react-spinners';
import { getProductsByAdmin, deleteProduct } from '../../actions/productAction';
// import UserForm from '../forms/userForm'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Material-UI
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grow from '@material-ui/core/Grow';
import MaterialTable, { MTableToolbar } from 'material-table';
import ProductModal from '../modals/ProductModal';
import { Grid } from '@material-ui/core'
import { Redirect } from 'react-router-dom';

class ProductListAdmin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Tên sản phẩm', field: 'productName' },
                { title: 'Giá', field: 'price' },
                { title: 'Tình trạng', field: 'isDeleted' },
            ],
            data: [],
        }
    }

    handleDelete(e, _id) {
        e.preventDefault();
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            this.props.deleteProduct(_id)
            this.props.getProductsByAdmin()
        }
    }

    componentDidMount() {
        this.props.getProductsByAdmin()
    }

    componentDidUpdate(prevProps) {
        const { products } = this.props;
        if (products !== prevProps.products) {

            let arr = [];
            products.forEach(product => {
                let status
                if (product.isDeleted)
                    status = 'Đã xóa'
                else
                    status = 'Tồn tại'

                const temp = {
                    _id: product._id,
                    urlImage: product.urlImage,
                    productName: product.productName,
                    price: '$' + product.price,
                    isDeleted: status
                }

                arr = [...arr, temp]
            });
            this.setState({
                data: arr
            })
        }
    }


    render() {
        const { data } = this.state;

        const loader = (
            <Container>
                <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                    <Col xs lg="2"></Col>
                    <Col md="auto">
                        <div style={{ float: 'center' }}>
                            <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                                <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                            </FadeLoader>
                        </div>
                    </Col>
                    <Col xs lg="2"></Col>
                </Row>
            </Container>
        )

        const table = (
            <div>
                <Grow in={true}>
                    <MaterialTable
                        title="DANH SÁCH SẢN PHẨM"
                        columns={this.state.columns}
                        data={this.state.data}
                        actions={[
                            {
                                icon: 'save',
                                tooltip: 'Save User',
                                Delete: (e, _id) => this.handleDelete(e, _id),
                            },

                        ]}
                        components={{
                            Toolbar: props => (
                                <div style={{ backgroundColor: '#e8eaf5' }}>
                                    <MTableToolbar {...props} />
                                </div>
                            ),
                            Action: props => {
                                return (
                                    <Grid
                                        container
                                        direction="row"
                                        justify="flex-start"
                                        alignItems="center"
                                        style={{ width: '30rem' }}
                                    >
                                        <Grid item>
                                            <ProductModal task='update' _id={props.data._id} />
                                        </Grid>

                                        <Grid item>
                                            {props.data.isDeleted === 'Đã xóa' ?
                                                <IconButton
                                                    disabled
                                                    aria-label="delete">
                                                    <DeleteIcon />
                                                </IconButton>
                                                :
                                                <IconButton
                                                    aria-label="delete"
                                                    style={{ color: '#ec2F4B' }}
                                                    onClick={(e) => props.action.Delete(e, props.data._id)}>
                                                    <DeleteIcon />
                                                </IconButton>
                                            }
                                        </Grid>
                                        <Grid item>
                                            <img src={props.data.urlImage} style={{ width: '8rem', marginLeft: '5rem' }}></img>
                                        </Grid>
                                    </Grid>
                                )
                            },
                        }}
                    />
                </Grow>
            </div>
        )

        return (
            <div>
                {
                    this.props.isAdmin ?
                        (this.props.products.length > 0 ? table : loader)
                        :
                        <Redirect to='/home' />
                }

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    products: state.product.products,
    loading: state.product.loading,
    error: state.error,
    isAdmin: state.auth.isAdmin
})

const mapDispatchToProps = {
    getProductsByAdmin,
    deleteProduct
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListAdmin)
