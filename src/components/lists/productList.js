import React, { Component, Fragment } from 'react';
import { Row, Col, ButtonGroup, Card, Modal, Form } from 'react-bootstrap';
import { getProductWithFilter } from '../../actions/productAction';
import { addToCart } from '../../actions/cartAction';
import { connect } from 'react-redux';
//Material-ui
// import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { getProductsByUser } from '../../actions/productAction'


class ProductList extends Component {
    constructor(props) {
        super(props);


        this.state = {
            productName: '',
            price: '',
            urlImage: '',
            // typeName: '',
            show: false
        }
    }

    componentDidMount() {
        // this.props.getProductsByUser()
    }


    addToCartHandler(e, _id) {
        e.preventDefault()
        this.props.addToCart(_id)
    }

    handleClose = () => {
        this.setState({ show: false })
    }

    handleShow(productName, price, urlImage) {
        this.setState({
            show: true,
            productName: productName,
            price: price,
            urlImage: urlImage
        })
    }

    render() {
        const { productsFiltered } = this.props;

        return (
            <div>
                <Row style={{ marginLeft: '1rem' }} >
                    {productsFiltered.length > 0 ? productsFiltered.map(product => {
                        const { _id, productName, price, urlImage } = product;
                        return (
                            <Col style={{ marginTop: '1rem' }}>
                                <Card style={{ width: 200 }} key={_id}>
                                    <CardActionArea onClick={() => this.handleShow(productName, price, urlImage)}>
                                        <Card.Img
                                            style={{ height: 180, width: 180 }}
                                            src={urlImage}
                                            title="Product"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                {productName}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                ${price}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                        <Grid
                                            container
                                            direction="row"
                                            justify="flex-end"
                                            alignItems="center"
                                        >
                                            <Grid item>
                                                <Button onClick={(e) => { this.addToCartHandler(e, _id) }} variant='outlined' color='secondary' size='small' > Thêm vào giỏ hàng </Button>
                                            </Grid>
                                        </Grid>
                                    </CardActions>
                                </Card>
                            </Col>
                        )
                    }) : null}
                </Row>

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>Chi tiết sản phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form >
                            <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                <Form.Label style={{ float: 'left' }}>Tên sản phẩm:</Form.Label>
                                <Form.Control type="text" name="productName" value={this.state.productName} />
                            </Form.Group>
                            <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                <Form.Label style={{ float: 'left' }}>Giá:</Form.Label>
                                <Form.Control type="text" name="price" value={this.state.price} />
                            </Form.Group>
                            <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                                <Form.Label style={{ float: 'left' }}>Hình ảnh sản phẩm:</Form.Label>
                                <Form.Control type="text" name="urlImage" value={this.state.urlImage} />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    loading: state.product.loading,
    productsFiltered: state.product.productsFiltered
})

const mapDispatchToProps = {
    getProductWithFilter,
    addToCart,
    getProductsByUser
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)