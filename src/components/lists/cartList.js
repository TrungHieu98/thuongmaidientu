import React, { Component, Fragment } from 'react'
import { Table, Form, Image, Row } from 'react-bootstrap'
import { BeatLoader } from 'react-spinners'
import { fetchOrderDetail, deleteItem, setQuantity, setProvisionalSum } from '../../actions/cartAction'
import { setTotal } from '../../actions/billAction'
import { connect } from 'react-redux'
// import PropTypes from 'prop-types';
// import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Delete'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import Card from '@material-ui/core/Card';


class CartList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            provisionalSum: ''
        }
    }

    componentDidMount() {
        this.props.fetchOrderDetail(this.props.items)
            .then(() => {
                this.setState({ state: this.state })
            })
    }

    componentDidUpdate(prevProps) {
        if (this.props.items !== prevProps.items) {
            console.log('changed')
            this.props.fetchOrderDetail(this.props.items)
        }
        if (prevProps.orderDetailsInCart !== this.props.orderDetailsInCart) {
            this.handleSum(this.props.orderDetailsInCart)
                .then(provisionalSum => {
                    this.props.setProvisionalSum(provisionalSum)
                    this.props.setTotal(0, provisionalSum)
                    this.setState({ provisionalSum: provisionalSum })
                })
        }
    }

    async setQuantity(index, value) {
        await this.props.setQuantity(index, value)
        // this.props.fetchOrderDetail(this.props.items)
    }

    handleSum(orderDetailsInCart) {
        return new Promise(resolve => {
            let provisionalSum = 0
            orderDetailsInCart.forEach(orderDetail => {
                const { unitPrice, quantity } = orderDetail
                provisionalSum += unitPrice * quantity
            })
            resolve(provisionalSum)
        })
    }

    async handleDelete(e, _id) {
        e.preventDefault()
        await this.props.deleteItem(_id, this.props.items)
        // this.props.fetchOrderDetail(this.props.items)
    }

    render() {
        const { orderDetailsInCart } = this.props

        const cartItems = (
            <Fragment>
                {orderDetailsInCart !== null ? orderDetailsInCart.map(orderDetail => {
                    const { productName, price, urlImage } = orderDetail.product

                    const key = Object.keys(orderDetailsInCart)[Object.values(orderDetailsInCart).indexOf(orderDetail)]
                    return (
                        <tr>
                            <td><Image src={urlImage} style={{ height: 50, width: 50 }}></Image></td>
                            <td>{productName}</td>
                            <td>${price}</td>
                            <td>
                                {/* <Form.Control style={{ width: 60 }} type='number'>
                                </Form.Control> */}
                                <Grid container
                                    direction="row"
                                    justify="center"
                                    alignItems="center">
                                    <Grid item>
                                        <p>{orderDetail.quantity}</p>
                                    </Grid>
                                    <Grid item>
                                        <ButtonGroup size="small" aria-label="small outlined button group">
                                            <Button onClick={() => this.setQuantity(key, 1)}>+</Button>
                                            <Button onClick={() => this.setQuantity(key, -1)}>-</Button>
                                        </ButtonGroup>
                                    </Grid>
                                </Grid>
                            </td>
                            <td>${price * orderDetail.quantity}</td>
                            <td></td>
                            <td>
                                <Button size="small" color="secondary" aria-label="delate" onClick={(e) => { this.handleDelete(e, key) }} >
                                    <AddIcon />
                                </Button>
                            </td>
                        </tr>
                    )
                }) : null}
            </Fragment>
        )

        return (
            <div>
                <Card>
                    <Form>
                        <Table hover responsive >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Tên sản phẩm:</th>
                                    <th>Đơn giá:</th>
                                    <th>Số lượng:</th>
                                    <th>Tổng</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {/* {isLoading ? loader : cartItems} */}
                                {cartItems}
                            </tbody>
                        </Table>

                        <Row style={{ float: "right", marginRight: '2rem' }}>
                            {/* <strong> */}
                            Tạm tính: ${this.state.provisionalSum}
                            {/* </strong> */}
                        </Row>
                    </Form>
                </Card>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    orderDetailsInCart: state.cart.orderDetailsInCart,
    isLoading: state.cart.isLoading,
    items: state.cart.items,
    error: state.error
})

const mapDispatchToProps = ({
    fetchOrderDetail,
    deleteItem,
    setProvisionalSum,
    setQuantity,
    setTotal
})

export default connect(mapStateToProps, mapDispatchToProps)(CartList)
