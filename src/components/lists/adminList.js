import React, { Component, Fragment } from 'react';
import { Table, Button, ButtonGroup } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import { BeatLoader } from 'react-spinners';
// action
import { getAdmins, deleteAdmin } from '../../actions/adminAction';
import { connect } from 'react-redux';
// import PropTypes from 'prop-types';

class AdminList extends Component {

    componentDidMount() {
        this.props.getAdmins();
    }

    // componentDidUpdate(prevProps) {
    //     if (this.props.admins !== prevProps.admins)
    //         this.props.getAdmins()
    // }

    handleDelete(_id) {
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            this.props.deleteAdmin(_id)
                .then(() => this.props.getAdmins())
        }
    }
    render() {
        const { admins, isAdmin } = this.props;

        const loadingComponent = (
            <Fragment>
                <td colSpan='5' align='center'>
                    <BeatLoader color="#50E3C2" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                        <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                    </BeatLoader>
                </td>
            </Fragment>
        )

        const loadedComponent = (
            <Fragment>
                {admins.map(admin => {
                    const { _id, userName, birthday, phone, address, email } = admin;
                    return (
                        <tr>
                            <td>{userName}</td>
                            <td>{birthday}</td>
                            <td>{phone}</td>
                            <td>{address}</td>
                            <td>{email}</td>
                            <td>
                                <ButtonGroup aria-label="Basic example">
                                    <Button onClick={() => { this.handleDelete(_id) }} variant="danger">Hủy quyền admin</Button>
                                </ButtonGroup>
                            </td>

                        </tr>
                    )
                })}
            </Fragment>
        )

        const table = (
            <Table hover responsive >
                <thead>
                    <tr>
                        <th>Tên admin</th>
                        <th>Ngày sinh</th>
                        <th>Số điện thoại</th>
                        <th>Địa chỉ</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {!this.props.loading ? loadedComponent : loadingComponent}
                </tbody>
            </Table>
        )

        return (
            <div>
                {
                    isAdmin ?
                        table
                        :
                        <Redirect to='/home' />
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    admins: state.admin.admins,
    loading: state.admin.loading,
    error: state.error,
    isAdmin: state.auth.isAdmin,
})

export default connect(mapStateToProps, { getAdmins, deleteAdmin })(AdminList)
