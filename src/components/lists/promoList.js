import React, { Component, Fragment } from 'react';
import { Row, Col, Container, Card } from 'react-bootstrap';
import { FadeLoader } from 'react-spinners';
import { getPromosByAdmin, updatePromo, deletePromo } from '../../actions/promoAction';
import { setupUpdateForm } from '../../actions/formAction';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Material-UI
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button'
import MaterialTable, { MTableToolbar } from 'material-table';
import PromoModal from '../modals/promoModal'

// class PromoList extends Component {

//     async handleDelete(e, _id) {
//         e.preventDefault()
//         // eslint-disable-next-line no-restricted-globals
//         if (confirm('Bạn thật sự muốn xóa?') === true) {
//             await this.props.deletePromo(_id)
//             this.props.getPromosByAdmin()
//         }
//     }

//     setupUpdateForm(e, _id) {
//         e.preventDefault()

//         this.props.setupUpdateForm(_id)
//     }

//     componentDidMount() {
//         console.log('promoList didmount')
//         this.props.getPromosByAdmin()
//     }


// componentDidUpdate(prevProps) {
//     if (this.props.promos !== prevProps.promos)
//     {
//         this.props.getPromosByAdmin()
//     }

//     // }

//     render() {
//         const { promos } = this.props;

//         const loadingComponent = (
//             <Fragment>
//                 <td colSpan='5' align='center'>
//                     <BeatLoader color="#50E3C2" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
//                         <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
//                     </BeatLoader>
//                 </td>
//             </Fragment>
//         )

//         const loadedComponent = (
//             <Fragment>
//                 {promos !== null ? promos.map(promo => {
//                     console.log('promoList render')
//                     const { _id, promoName, percent } = promo;

//                     return (
//                         <tr>
//                             <td>{promoName}</td>
//                             <td>{percent}</td>
//                             <td>
//                                 <ButtonGroup aria-label="Basic example">
//                                     <Button onClick={(e) => { this.setupUpdateForm(e, _id) }} variant="secondary">Sửa</Button>
//                                     <Button onClick={(e) => { this.handleDelete(e, _id) }} variant="danger">Xóa</Button>
//                                 </ButtonGroup>
//                             </td>
//                         </tr>
//                     )
//                 }) : null}
//             </Fragment>
//         )

//         return (
//             <div className="shadow-lg">
//                 {/* <Button variant="outline-primary">Create</Button> */}
//                 <Table hover responsive >
//                     <thead>
//                         <tr>
//                             <th>Tên khuyến mãi</th>
//                             <th>Phần trăm khuyến mãi</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {this.props.loading ? loadingComponent : loadedComponent}
//                     </tbody>
//                 </Table>
//             </div>
//         )

//     }
// }

// const mapStateToProps = state => ({
//     promos: state.promo.promos,
//     loading: state.promo.loading,
//     error: state.error
// })

// export default connect(mapStateToProps, { getPromosByAdmin, deletePromo, setupUpdateForm })(PromoList)

export class PromoList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Tên khuyến mãi', field: 'promoName' },
                { title: 'Phần trăm khuyến mãi', field: 'percent' },
                { title: 'Giá trị giảm tối đa', field: 'maxDiscount' },
                { title: 'Giá trị đơn hàng tối thiểu', field: 'minOrderPrice' },
                { title: 'Số lượng còn lại', field: 'quantity' },
                { title: 'Ngày hết hạn', field: 'expiredDate' },
                { title: 'Nhà cung cấp', field: 'producer' },
                { title: 'Tình trạng', field: 'isDeleted' }
            ],
            data: [],
        }
    }
    
    handleDelete(e, _id) {
        e.preventDefault();
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            this.props.deletePromo(_id)
            this.props.getPromosByAdmin()
        }
    }

    componentDidMount() {
        this.props.getPromosByAdmin()
    }

    componentDidUpdate(prevProps) {
        const { promos } = this.props;
        if (promos !== prevProps.promos) {

            let arr = [];
            promos.forEach(promo => {
                let status
                if (promo.isDeleted)
                    status = 'Đã xóa'
                else
                    status = 'Tồn tại'

                const temp = {
                    _id: promo._id,
                    promoName: promo.promoName,
                    percent: promo.percent,
                    maxDiscount: promo.maxDiscount,
                    minOrderPrice: promo.minOrderPrice,
                    quantity: promo.quantity,
                    producer: promo.producer,
                    expiredDate: promo.expiredDate,
                    isDeleted: status
                }

                arr = [...arr, temp]
            })
            this.setState({
                data: arr
            })
        }
    }

    render() {
        const { data } = this.state;

        if (data.length > 0) {
            return (
                <div>
                    <Grow in={true}>
                        <MaterialTable
                            title="DANH SÁCH KHUYẾN MÃI"
                            columns={this.state.columns}
                            data={this.state.data}
                            actions={[
                                {
                                    icon: 'save',
                                    tooltip: 'Save User',
                                    Delete: (e, _id) => this.handleDelete(e, _id),
                                },
                            ]}
                            components={{
                                Toolbar: props => (
                                    <div style={{ backgroundColor: '#e8eaf5' }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                                Action: props => {
                                    console.log(props.data)
                                    return (
                                        <Row>
                                            <PromoModal task='update' _id={props.data._id} />
                                            {props.data.isDeleted === 'Đã xóa' ?
                                                null
                                                :
                                                <IconButton aria-label="delete" style={{ color: '#ec2F4B' }}
                                                    onClick={(e) => props.action.Delete(e, props.data._id)}
                                                // isDisable='true'
                                                >
                                                    <DeleteIcon />
                                                </IconButton>
                                            }
                                        </Row>
                                    )
                                }
                            }}
                        />
                    </Grow>
                </div>
            )
        }
        else {
            return (
                <Container>
                    <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                        <Col xs lg="2"></Col>
                        <Col md="auto">
                            <div style={{ float: 'center' }}>
                                <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                                    <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                                </FadeLoader>
                            </div>

                        </Col>
                        <Col xs lg="2"></Col>
                    </Row>
                </Container>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    promos: state.promo.promos,
    loading: state.promo.loading,
    error: state.error
})

const mapDispatchToProps = ({
    getPromosByAdmin,
    deletePromo
})

export default connect(mapStateToProps, mapDispatchToProps)(PromoList)