import React, { Component, Fragment } from 'react';
import { Table, Button, ButtonGroup, Row, Col, Container } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import { FadeLoader } from 'react-spinners';
import { getUsers, deleteUser } from '../../actions/userAction';
import UserForm from '../forms/userForm'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Material-UI
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Grow from '@material-ui/core/Grow';
import MaterialTable, { MTableToolbar } from 'material-table';

class UserList extends Component {

    // componentDidMount() {
    //     this.props.getUsers();
    //     console.log("from list user: ", this.props.users)
    // }
    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {},
                { title: 'Tên người dùng', field: 'userName' },
                { title: 'Ngày sinh', field: 'birthday' },
                { title: 'Sđt', field: 'phone' },
                { title: 'Địa chỉ', field: 'address' },
                { title: 'Email', field: 'email' }
            ],
            data: [],
        }
    }
    async handleDelete(e, _id) {
        e.preventDefault();
        // eslint-disable-next-line no-restricted-globals
        if (confirm('Bạn thật sự muốn xóa?') === true) {
            await this.props.deleteUser(_id)
            this.props.getUsers()
        }
    }
    static propTypes = {
        users: PropTypes.array.isRequired,
        getUsers: PropTypes.func.isRequired
    }
    async componentDidMount() {
        await this.props.getUsers()
            .then(() => {
                const { users } = this.props;

                let newUsers = [];
                users.forEach(user => {
                    const newUser = {
                        userName: user.userName,
                        birthday: user.birthday,
                        phone: user.phone,
                        address: user.address,
                        email: user.email
                    }
                    newUsers = [...newUsers, newUser]
                });
                this.setState({
                    data: newUsers
                })
            })
    }

    render() {
        const { data } = this.state;
        if (data.length > 0) {
            return (
                <div>
                    <Grow in={true}>
                        <MaterialTable
                            title="DANH SÁCH NGƯỜI DÙNG"
                            columns={this.state.columns}
                            data={this.state.data}
                            actions={[
                                {
                                    icon: 'save',
                                    tooltip: 'Save User',
                                    Update: (e, rowData) => alert("You updated " + rowData.name),
                                    Delete: (e, rowData) => alert("You deleted " + rowData.name),
                                },

                            ]}
                            components={{
                                Toolbar: props => (
                                    <div style={{ backgroundColor: '#e8eaf5' }}>
                                        <MTableToolbar {...props} />
                                    </div>
                                ),
                                Action: props => (
                                    <Row>
                                        <IconButton aria-label="edit" style={{ color: '#009FFF' }}
                                            onClick={(event) => props.action.Update(event, props.data)}>
                                            <EditIcon />
                                        </IconButton>
                                        <IconButton aria-label="delete" style={{ color: '#ec2F4B' }}
                                            // onClick={(e) => { this.handleDelete(e, _id) }}
                                            onClick={(event) => props.action.Update(event, props.data)}
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </Row>
                                ),

                            }}
                        />
                    </Grow>
                </div>
            )
        }
        else {
            return (
                <Container>
                    <Row style={{ marginTop: '13rem' }} className="justify-content-md-center">
                        <Col xs lg="2"></Col>
                        <Col md="auto">
                            <div style={{ float: 'center' }}>
                                <FadeLoader color="#33bbff" size="60" animation="border" role="status" style={{ height: '10vh', width: '10vh' }} >
                                    <span className="sr-only"><strong style={{ fontSize: '5vh' }}>Loading...</strong></span>
                                </FadeLoader>
                            </div>

                        </Col>
                        <Col xs lg="2"></Col>
                    </Row>
                </Container>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    users: state.user.users,
    loading: state.user.loading,
    error: state.error
})

const mapDispatchToProps = {
    getUsers,
    deleteUser
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
