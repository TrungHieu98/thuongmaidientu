import React, { Component } from 'react'
import { connect } from 'react-redux';
// import InputLabel from '@material-ui/core/InputLabel';
// import MenuItem from '@material-ui/core/MenuItem';
// import FormControl from '@material-ui/core/FormControl';
// import Select from '@material-ui/core/Select';
import { Form } from 'react-bootstrap'
import { getPromosOfUser, setPromoSelected } from '../actions/promoAction'
import { calculateAndSetDiscount, setTotal, setDiscountValue } from '../actions/billAction'
import { removePromoUsed } from '../actions/authAction'
import { Button } from '@material-ui/core';

class PromoSelection extends Component {
    constructor(props) {
        super(props)
        this.state = {
            promoSelected: '',
        }
    }

    componentDidMount() {
        this.props.getPromosOfUser(this.props.user.promos)
    }

    async componentDidUpdate(prevProps, prevState) {
        if (this.state.promoSelected !== prevState.promoSelected) {
            if (this.state.promoSelected === 'none') {
                this.props.setPromoSelected(null)
                this.props.setDiscountValue(0)
                this.props.setTotal(0, this.props.provisionalSum)
            }
            else {
                this.props.setPromoSelected(this.state.promoSelected)
                const discount = await this.props.calculateAndSetDiscount(this.state.promoSelected, this.props.provisionalSum)
                this.props.setTotal(discount, this.props.provisionalSum)
            }
        }
        if (this.props.user.promos !== prevProps.user.promos) {
            this.props.getPromosOfUser(this.props.user.promos)
        }
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: JSON.parse(e.target.value)
        })
    }

    render() {
        return (
            <Form.Group>
                <Form.Label style={{ float: 'left' }}>Áp dụng khuyến mãi:</Form.Label>
                <Form.Control name="promoSelected" as="select" multiple onChange={(e) => this.changeHandler(e)}>
                    <option value={JSON.stringify('none')}>Không sử dụng khuyến mãi</option>
                    {this.props.userPromos.map(promo => {
                        return (
                            <option value={JSON.stringify(promo)}>{promo.promoName} - Giảm {promo.percent}%</option>
                        )
                    })}
                </Form.Control>
                <Button
                    onClick={() => {
                        this.props.promoSelected !== null ?
                            this.props.removePromoUsed(this.props.promoSelected._id, this.props.user)
                            : alert('Không phải khuyến mãi')
                    }}
                    variant='outlined'
                    color='secondary'
                    size='small'
                >
                    Áp dụng
                </Button>
            </Form.Group >
            // <FormControl variant="filled">
            //     <InputLabel id="demo-simple-select-filled-label">Age</InputLabel>
            //     <Select
            //         labelId="demo-simple-select-filled-label"
            //         id="demo-simple-select-filled"
            //         value={this.state.promo_id}
            //     // onChange={}
            //     >
            //         <MenuItem value="">
            //             <em>Không sử dụng</em>
            //         </MenuItem>
            //         <MenuItem value={10}>Ten</MenuItem>
            //         <MenuItem value={20}>Twenty</MenuItem>
            //         <MenuItem value={30}>Thirty</MenuItem>
            //     </Select>
            // </FormControl>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    userPromos: state.promo.userPromos,
    promoSelected: state.promo.promoSelected,
    provisionalSum: state.cart.provisionalSum
})

const mapDispatchToProps = ({
    getPromosOfUser,
    setPromoSelected,
    calculateAndSetDiscount,
    setTotal,
    setDiscountValue,
    removePromoUsed
})

export default connect(mapStateToProps, mapDispatchToProps)(PromoSelection);