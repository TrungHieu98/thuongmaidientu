import React, { Component, Fragment } from 'react'
import { logout } from '../actions/authAction'
import { connect } from 'react-redux'
import { Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
// import PropTypes from 'prop-types'

export class Logout extends Component {

    handleLogout = () => {
        this.props.logout()
        window.location.href = '/home'
    }

    render() {
        return (
            <div>
                <Nav.Link onClick={() => this.handleLogout()} to={'/'}>
                    Đăng xuất
                </Nav.Link>
                {/* <IconButton onClick={this.props.logout} style={{ backgroundColor: '#fafafa' }} size='medium' aria-label="sign out">
                    <ExitToAppIcon to={'/homepage'}/>
                </IconButton> */}
            </div>
        )
    }
}

export default connect(null, { logout })(Logout)