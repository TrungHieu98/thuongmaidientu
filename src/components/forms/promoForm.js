import React, { Component } from 'react'
// import { Form, Button } from 'react-bootstrap'
// import { connect } from 'react-redux'
// import { Link } from 'react-router-dom'
// import { createPromo, getPromo, updatePromo } from '../../actions/promoAction'
// import { clearErrors } from '../../actions/errorAction'
// import { setupCreateForm } from '../../actions/formAction'
// import { resolve } from 'path'
import PromoModal from '../modals/promoModal'

class PromoForm extends Component {
    render() {
        return (
            <PromoModal task='add' />
        )
    }
}

export default PromoForm

// class Promo extends Component {
//     constructor(props) {
//         super(props);

//         this.handleSubmit = this.handleSubmit.bind(this);

//         this.state = {
//             promoName: '',
//             percent: '',

//             msg: null,
//             show: false
//         }
//     }

//     setupCreateForm(e, id) {
//         e.preventDefault()
//         this.props.setupCreateForm()
//         this.resetForm()
//     }

//     // getPromoInfo = (e) => {
//     //     e.preventDefault()
//     //     this.props.getPromo()
//     // }
//     resetForm() {
//         this.setState({
//             promoName: '',
//             percent: ''
//         })
//         this.forceUpdate()
//     }

//     handleSubmit = (e) => {
//         e.preventDefault();
//         const newPromo = {
//             promoName: this.state.promoName,
//             percent: this.state.percent,
//         }
//         if (this.props.updating) {
//             this.props.updatePromo(this.props.id, newPromo)
//         }
//         else {
//             this.props.createPromo(newPromo)
//         }
//     }

//     changeHandler = (e) => {
//         this.setState({
//             [e.target.name]: e.target.value
//         })
//     }

//     async setValueForInput() {
//         await this.props.getPromo(this.props.id)
//             .then(() => {
//                 const { promo } = this.props

//                 this.setState({
//                     promoName: promo.promoName,
//                     percent: promo.percent
//                 })
//                 this.forceUpdate()
//             })
//     }


//     componentDidUpdate(prevProps) {
//         const { error, updating } = this.props;
//         if (error !== prevProps.error) {
//             if (error.id === 'CREATE_PROMO_FAIL') {
//                 this.setState({ msg: error.msg.msg });
//             } else {
//                 this.setState({ msg: null });
//             }
//         }

//         if (updating !== prevProps.updating)
//             if (updating) {
//                 this.setValueForInput()
//             }
//     }

//     render() {

//         const createButton = (
//             <Button style={{marginBottom:'1rem'}} variant="primary" type="submit">Tạo mới</Button>
//         )

//         const updateButton = (
//             <div>
//                 <Button style={{marginBottom:'1rem', marginRight:'1rem'}} variant="success" type="submit">Cập nhật</Button>
//                 <Button style={{marginBottom:'1rem'}} variant="danger" onClick={(e) => { this.setupCreateForm(e) }}>Hủy</Button>
//             </div>
//         )

//         return (

//             <Form onSubmit={this.handleSubmit}>
//                 <Form.Label className="font-weight-bold">THÔNG TIN KHUYẾN MÃI</Form.Label>

//                 {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

//                 <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
//                     <Form.Label style={{ float: 'left' }}>Tên khuyến mãi:</Form.Label>
//                     <Form.Control type="text" value={this.state.promoName} name="promoName" onChange={(e) => this.changeHandler(e)} />
//                 </Form.Group>

//                 <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
//                     <Form.Label style={{ float: 'left' }}>Phần trăm:</Form.Label>
//                     <Form.Control type="text" value={this.state.percent} name="percent" onChange={(e) => this.changeHandler(e)} />
//                 </Form.Group>

//                 {this.props.updating ? updateButton : createButton}

//             </Form>
//         )
//     }
// }

// const mapStateToProps = (state) => ({
//     promo: state.promo.promo,
//     id: state.form.id,
//     updating: state.form.updating,
//     error: state.error

// })

// // const mapDispatchToProps = (dispatch) => ({
// //     register: () => dispatch(register())
// // })

// export default connect(mapStateToProps, { createPromo, updatePromo, clearErrors, setupCreateForm, getPromo })(Promo);