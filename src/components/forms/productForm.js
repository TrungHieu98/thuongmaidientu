import React, { Component } from 'react'
import ProductModal from '../modals/ProductModal';

class ProductForm extends Component {
    render() {
        return (
            <ProductModal task='add' />
        )
    }
}

export default ProductForm

// class ProductForm extends Component {
//     constructor(props) {
//         super(props);

//         this.handleSubmit = this.handleSubmit.bind(this);

//         this.state = {
//             productName: '',
//             price: '',
//             urlImage: '',
//             msg: null
//         }
//     }

//     resetForm() {
//         this.setState({
//             productName: '',
//             price: '',
//             urlImage: ''
//         })
//     }

//     handleSubmit(e) {
//         e.preventDefault();

//         const newProduct = {
//             productName: this.state.productName,
//             price: this.state.price,
//             urlImage: this.state.urlImage
//         }
//         if (this.props.updating) {
//             this.props.updateProduct(this.props.id, newProduct)
//         }
//         else {
//             this.props.createProduct(newProduct)
//         }
//     }
//     changeHandler = (e) => {
//         this.setState({
//             [e.target.name]: e.target.value
//         })
//     }

//     async setValueForInput() {
//         await this.props.getProduct(this.props.id)
//             .then(() => {
//                 const { product } = this.props
//                 this.setState({
//                     productName: product.productName,
//                     price: product.price,
//                     urlImage: product.urlImage
//                 })
//                 this.forceUpdate()
//             })
//     }

//     componentDidUpdate(prevProps) {
//         const { error, updating } = this.props;
//         if (error !== prevProps.error) {
//             if (error.id === 'CREATE_PRODUCT_FAIL') {
//                 this.setState({ msg: error.msg.msg });
//             } else {
//                 this.setState({ msg: null });
//             }
//         }

//         if (updating !== prevProps.updating)
//             if (updating) {
//                 this.setValueForInput()
//             }
//     }

//     render() {
//         const createButton = (
//             <Button style={{ marginBottom: '1rem' }} variant="primary" type="submit">Tạo mới</Button>
//         )

//         const updateButton = (
//             <div>
//                 <Button style={{ marginBottom: '1rem', marginRight: '1rem' }} variant="success" type="submit">Cập nhật</Button>
//                 <Button style={{ marginBottom: '1rem' }} variant="danger" onClick={() => { this.props.setupCreateForm(); this.resetForm() }}>Hủy</Button>
//             </div >
//         )

//         return (
//             <Form onSubmit={this.handleSubmit}>
//                 <Form.Label className="font-weight-bold">THÔNG TIN SẢN PHẨM</Form.Label>

//                 {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

//                 <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
//                     <Form.Label style={{ float: 'left' }}>Tên sản phẩm:</Form.Label>
//                     <Form.Control type="text" name="productName" value={this.state.productName} onChange={(e) => this.changeHandler(e)} />
//                 </Form.Group>

//                 <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
//                     <Form.Label style={{ float: 'left' }}>Giá:</Form.Label>
//                     <Form.Control type="text" name="price" value={this.state.price} onChange={(e) => this.changeHandler(e)} />
//                 </Form.Group>

//                 <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
//                     <Form.Label style={{ float: 'left' }}>Hình ảnh sản phẩm:</Form.Label>
//                     <Form.Control type="text" name="urlImage" value={this.state.urlImage} onChange={(e) => this.changeHandler(e)} />
//                 </Form.Group>

//                 {this.props.updating ? updateButton : createButton}

//                 <ProductModal task='add'/>
//             </Form>
//         )
//     }
// }

// const mapStateToProps = (state) => ({
//     product: state.product.product,
//     id: state.form.id,
//     updating: state.form.updating,
//     error: state.error
// })

// // const mapDispatchToProps = (dispatch) => ({
// //     register: () => dispatch(register())
// // })

// export default connect(mapStateToProps, { getProduct, createProduct, updateProduct, setupCreateForm, clearErrors })(ProductForm);