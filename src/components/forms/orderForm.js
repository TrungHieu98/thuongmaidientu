import React, { Component } from 'react'
import { Form, Button, Modal, Alert } from 'react-bootstrap'
import { connect } from 'react-redux'
import { clearErrors } from '../../actions/errorAction'
// import { updateOrder, getOrder } from '../../actions/orderAction'

class OrderForm extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            _id: props._id,
            userName: '',
            orderDate: '',
            shipDate: '',
            provisionalSum: '',
            email: '',
            msg: null,
            show: false
        }
        // this.setValueForInput()
    }

    handleSubmit = (e) => {
        e.preventDefault();

        // const newData = {
        //     birthday: this.state.birthday,
        //     phone: this.state.phone,
        //     address: this.state.address,
        //     email: this.state.email,
        // }

        // this.props.updateUser(this.state._id, newData)
        // this.setState({ show: false })
    }

    setValueForInput() {
        // this.props.getUser(this.state._id)
        //     .then(() => {
        //         const { userName, birthday, phone, address, email } = this.props.user
        //         this.setState({
        //             _id: this.state._id,
        //             userName: userName,
        //             birthday: birthday,
        //             phone: phone,
        //             address: address,
        //             email: email
        //         })
        //     })
    }

    componentDidUpdate(prevProps) {
        // const { error } = this.props;
        // if (error !== prevProps.error) {
        //     if (error.id === 'UPDATE_USER_FAIL') {
        //         this.setState({ msg: error.msg.msg });
        //     } else {
        //         this.setState({ msg: null });
        //     }
        // }
    }

    handleClose = () => {
        this.props.clearErrors()
        this.setState({ show: false })
    }

    handleShow() {
        this.setState({ show: true })
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <Button onClick={() => this.handleShow()} >Sửa</Button>

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>CẬP NHẬT THÔNG TIN NGƯỜI DÙNG</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Tên người dùng:</Form.Label>
                                <Form.Control type="text" value={this.state.userName} name="userName" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Ngày đặt:</Form.Label>
                                <Form.Control type="date" value={this.state.orderDate} name="orderDate" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Ngày giao:</Form.Label>
                                <Form.Control type="date" value={this.state.shipDate} name="shipDate" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label style={{ float: 'left' }}>Tổng tiền:</Form.Label>
                                <Form.Control type="text" value={this.state.provisionalSum} name="provisionalSum" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Xác nhận
                            </Button>
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user.user,
    error: state.error

})

export default connect(mapStateToProps, { clearErrors })(OrderForm)