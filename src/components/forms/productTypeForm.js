import React, { Component, Alert } from 'react'
import ProductTypeModal from '../modals/productTypeModal'
// import { Navbar, Nav, Button, Dropdown, NavDropdown, Form, Col } from 'react-bootstrap'
// import { connect } from 'react-redux'
// import { Link, Route, Switch } from 'react-router-dom'
// import {createType, getType, updateType} from '../../actions/productTypeAction';
// import { setupCreateForm } from '../../actions/formAction'
// import {clearErrors} from '../../actions/errorAction';

class ProductType extends Component {
    render() {
        return (
            <ProductTypeModal task='add' />
        )
    }
}

export default ProductType
// class ProductType extends Component {
//     constructor(props) {
//         super(props);

//         this.handleSubmit = this.handleSubmit.bind(this);

//         this.state = {
//             typeName: '',
//             msg: null,
//         }
//     }

//     setupCreateForm(e, id) {
//         e.preventDefault()
//         this.props.setupCreateForm()
//         this.resetForm()
//     }

//     resetForm() {
//         this.setState({
//             typeName: ''
//         })
//         this.forceUpdate()
//     }

//     handleSubmit = (e) => {
//         e.preventDefault();
//         const newProductType = {
//             typeName: this.state.typeName
//         }
//         if (this.props.updating) {
//             this.props.updateType(this.props.id, newProductType)
//         }
//         else {
//             this.props.createType(newProductType)
//         }
//     }

//     changeHandler = (e) => {
//         this.setState({
//             [e.target.name]: e.target.value
//         })
//     }

//     async setValueForInput() {
//         await this.props.getType(this.props.id)
//             .then(() => {
//                 const { productType } = this.props
//                 this.setState({
//                     typeName: productType.typeName
//                 })
//                 this.forceUpdate()
//             })
//     }

//     componentDidUpdate(prevProps) {
//         const { error, updating } = this.props;
//         if (error !== prevProps.error) {
//             if (error.id === 'CREATE_TYPE_FAIL') {
//                 this.setState({ msg: error.msg.msg });
//             } else {
//                 this.setState({ msg: null });
//             }
//         }

//         if (updating !== prevProps.updating)
//             if (updating) {
//                 this.setValueForInput()
//             }
//     }

//     render() {
//         const createButton = (
//             <Button style={{marginBottom:'1rem'}} variant="primary" type="submit">Tạo mới</Button>
//         )

//         const updateButton = (
//             <div>
//                 <Button style={{marginBottom:'1rem', marginRight:'1rem'}} variant="success" type="submit">Cập nhật</Button>
//                 <Button style={{marginBottom:'1rem'}} variant="danger" onClick={(e) => { this.setupCreateForm(e) }}>Hủy</Button>
//             </div>
//         )
//         return (
//             <div >
//                 <Form onSubmit={this.handleSubmit}>
//                     <Form.Label className="font-weight-bold">THÔNG TIN LOẠI SẢN PHẨM</Form.Label>

//                     {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

//                     <Form.Group style={{marginLeft:'1rem', marginRight:'1rem'}}>
//                         <Form.Label style={{ float: 'left' }}>Tên loại sản phẩm:</Form.Label>
//                         <Form.Control type="text" value={this.state.typeName} name="typeName" onChange={(e) => this.changeHandler(e)} />
//                     </Form.Group>

//                     {this.props.updating ? updateButton : createButton}
//                 </Form>
//             </div>
//         )
//     }
// }

// const mapStateToProps = (state) => ({
//     productType: state.productType.productType,
//     id: state.form.id,
//     updating: state.form.updating,
//     error: state.error
// })

// // const mapDispatchToProps = (dispatch) => ({
// //     register: () => dispatch(register())
// // })

// export default connect(mapStateToProps, { getType ,createType, updateType, setupCreateForm,clearErrors })(ProductType);