import React, { Component, Fragment } from 'react'
import { Form, NavDropdown, Row, Col } from 'react-bootstrap'
import { connect } from 'react-redux'
import { getProductWithFilter, getProductsByUser } from '../../actions/productAction'
import Button from '@material-ui/core/Button'
import SearchIcon from '@material-ui/icons/Search';
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'

class SearchBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: '',
        }
    }

    changeHandler = (e) => {
        e.preventDefault()
        this.setState({
            filter: e.target.value
        })
    }

    async componentDidMount() {
        const products = await this.props.getProductsByUser()
        console.log(products)
        this.props.getProductWithFilter(this.state.filter, products)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.filter !== prevState.filter) {
            this.props.getProductWithFilter(this.state.filter, this.props.products)
        }
    }

    render() {
        return (
            // <Grid container>
            <Card style={{ padding: '0.5rem', backgroundColor: '#202932' }}>
                <Grid container>
                    <Grid item xs>
                        <SearchIcon />
                    </Grid>
                    <Grid item xs={10} style={{ width: '40rem' }}>
                        <Form.Control type="text" placeholder="Search..." name='filter' onChange={(e) => this.changeHandler(e)} />
                    </Grid>
                </Grid>
            </Card>
            // </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    // productsFiltered: state.product.productsFiltered,
    error: state.error,
    products: state.product.products,
})

const mapDispatchToProps = {
    getProductWithFilter,
    getProductsByUser
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);

