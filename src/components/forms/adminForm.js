import React, { Component } from 'react'
import { Alert, Button, Form, Card } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addAdmin } from '../../actions/adminAction'
import { clearErrors } from '../../actions/errorAction'

class AdminForm extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            msg: null,
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.email)
        this.props.addAdmin(this.state.email)
    }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            if (error.id === 'ADD_ADMIN_FAIL') {
                this.setState({ msg: error.msg.msg });
            } else {
                this.setState({ msg: null });
            }
        }
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit} >
                    <Form.Label className="font-weight-bold">Thêm Admin</Form.Label>
                    {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}
                    <Form.Group style={{ marginLeft: '1rem', marginRight: '1rem' }}>
                        <Form.Label style={{ float: 'left' }}>Email:</Form.Label>
                        <Form.Control type="text" name="email" onChange={(e) => this.changeHandler(e)} />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Xác nhận
                    </Button>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    error: state.error
})

export default connect(mapStateToProps, { addAdmin, clearErrors })(AdminForm);