import React, { Component, Fragment } from 'react'
import { Carousel, Container } from 'react-bootstrap'

class Banner extends Component {
    render() {
        return (
            <Container style={{ marginTop: '0.5rem' }}>
                <Carousel>
                    <Carousel.Item>
                        <img style={{ height: '100%', maxHeight: '23rem' }}
                            className="d-block w-100"
                            src="https://image.freepik.com/free-vector/back-school-with-school-items-elements_3589-792.jpg"
                            // src={Image1}
                            alt="First slide"
                        />
                    </Carousel.Item>

                    <Carousel.Item>
                        <img style={{ height: '100%', maxHeight: '23rem' }}
                            className="d-block w-100"
                            src="http://www.kingstarbags.com/images/products/backpack-banner.jpg"
                            // src={Image2}
                            alt="Second slide"
                        />

                    </Carousel.Item>

                    <Carousel.Item>
                        <img style={{ height: '100%', maxHeight: '23rem' }}
                            className="d-block w-100"
                            src="https://image.freepik.com/free-vector/back-school-accesories_86693-134.jpg"
                            alt="Third slide"
                        />
                    </Carousel.Item>

                    <Carousel.Item>
                        <img style={{ height: '100%', maxHeight: '23rem' }}
                            className="d-block w-100"
                            src="https://image.freepik.com/free-vector/sale-banner-template-design_74379-121.jpg"
                            // src={Image4}
                            alt="Fourth slide"
                        />

                    </Carousel.Item>
                </Carousel>
            </Container>
        )
    }
}

export default Banner