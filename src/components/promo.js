import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { getPromoByUser, takePromo } from '../actions/promoAction'

class Promo extends Component {

    componentDidMount() {
        this.props.getPromoByUser()
    }

    takePromoHandler = async (promo_id) => {
        if (this.props.isAuthenticated)
            this.props.takePromo(this.props.user._id, promo_id)
        else
            alert('Bạn cần đăng nhập để nhận mã khuyến mãi')
    }

    render() {
        const promoCard = (promo) => {
            return (
                <Card>
                    <CardActionArea>
                        <Grid
                            container
                            direction="column"
                            justify="center"
                            alignItems="center"
                            spacing={2}
                        >
                            <Grid item></Grid>
                            <Grid item>
                                <CardMedia style={{ maxWidth: '10rem', height: '10rem' }}>
                                    <img
                                        width='80%'
                                        src={promo.urlImage}
                                        alt='promo' />
                                </CardMedia>
                            </Grid>
                        </Grid>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {promo.promoName}
                            </Typography>
                            <Grid
                                container
                                direction="column"
                                justify="flex-start"
                                alignItems="flex-start"
                            >
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Phần trăm khuyến mãi :</strong>
                                    {promo.percent}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Mô tả :</strong>
                                    {promo.description}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Giá trị giảm tối đa :</strong>
                                    {promo.maxDiscount}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Giá trị đơn hàng tối thiểu :</strong>
                                    {promo.minOrderPrice}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Nhà cung cấp :</strong>
                                    {promo.producer}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    <strong>Ngày hết hạn :</strong>
                                    {promo.expiredDate}
                                </Typography>
                            </Grid>
                        </CardContent>
                    </CardActionArea>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="flex-start"
                    >
                        <Grid item style={{ padding: '1rem' }}>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <strong>Còn lại :</strong>
                                {promo.quantity}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <CardActions>
                                {/* {userHasPromo ?
                                    <p>Bạn đã nhận mã này trước đây</p>
                                    : */}
                                <Button size="small" variant='outlined' color="secondary"
                                    onClick={() => { this.takePromoHandler(promo._id) }}
                                >
                                    Nhận ngay
                                    </Button>
                                {/* } */}
                            </CardActions>
                        </Grid>
                    </Grid>
                </Card >
            )
        }

        return (
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
                spacing={4}
            >
                {
                    this.props.promosClient.map(promo => {
                        return (
                            <Grid item>
                                {promoCard(promo)}
                            </Grid>
                        )
                    })
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    promosClient: state.promo.promosClient,
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user
})

const mapDispatchToProps = ({
    getPromoByUser,
    takePromo,
    // takePromo,
    // checkUserRecord
    // testDuplicatePromo
})

export default connect(mapStateToProps, mapDispatchToProps)(Promo);