import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Nav, Alert } from 'react-bootstrap';
// import PropTypes from 'prop-types';
import { register } from '../actions/authAction'
import { clearErrors } from '../actions/errorAction'

//Material-UI
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import Button from '@material-ui/core/Button';

class RegisterModal extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            userName: '',
            birthday: '',
            phone: '',
            address: '',
            email: '',
            password: '',
            msg: null,
            show: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const newUser = {
            userName: this.state.userName,
            birthday: this.state.birthday,
            phone: this.state.phone,
            address: this.state.address,
            email: this.state.email,
            password: this.state.password,
        }
        //console.log(newUser)
        this.props.register(newUser)
    }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            // kiem tra registor error
            if (error.id === 'REGISTER_FAIL') {
                this.setState({ msg: error.msg.msg });
            } else {
                this.setState({ msg: null });
            }
        }

        // console.log('isAuthenticated: ', this.props.isAuthenticated)
        // console.log('show :', this.state.show)
        if (this.state.show) {
            if (this.props.isAuthenticated) {
                this.handleClose();
            }
        }
    }

    handleClose = () => {
        this.props.clearErrors();
        this.setState({ show: false });
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div>
                <Nav.Link onClick={() => this.handleShow()} href="#">
                    Đăng ký
                </Nav.Link>
                {/* <Button
                    onClick={() => this.handleShow()}
                    variant="contained"
                    color="default"
                    endIcon={<AssignmentIndIcon href="#" />}
                >d
                    Đăng ký
                </Button> */}

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>Đăng ký</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Tên:</Form.Label>
                                <Form.Control type="text" name="userName" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicBirthday">
                                <Form.Label>Ngày sinh:</Form.Label>
                                <Form.Control type="text" name="birthday" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPhonenumber">
                                <Form.Label>Số điện thoại:</Form.Label>
                                <Form.Control type="text" name="phone" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicAddress">
                                <Form.Label>Địa chỉ:</Form.Label>
                                <Form.Control type="text" name="address" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email:</Form.Label>
                                <Form.Control type="email" name="email" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Mật khẩu:</Form.Label>
                                <Form.Control type="password" name="password" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Button variant="contained" color="primary" type="submit" >Xác nhận</Button>
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error

})

// const mapDispatchToProps = (dispatch) => ({
//     register: () => dispatch(register())
// })

export default connect(mapStateToProps, { register, clearErrors })(RegisterModal);
