import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Nav, Alert } from 'react-bootstrap';
// import PropTypes from 'prop-types';
import { login } from '../actions/authAction'
import { clearErrors } from '../actions/errorAction'
//Material-UI
// import LockOpenIcon from '@material-ui/icons/LockOpen';
import Button from '@material-ui/core/Button';

class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {

            email: '',
            password: '',
            msg: null,
            show: false
        }
    }
    // static proprTypes = {
    //     isAuthenticated: PropTypes.bool,
    //     error: PropTypes.object.isRequired,
    //     login: PropTypes.func.isRequired,
    //     clearErrors: PropTypes.func.isRequired
    // }

    handleSubmit = (e) => {
        e.preventDefault();

        const user = {
            email: this.state.email,
            password: this.state.password
        }
        console.log('from submit handler :', user);
        
        this.props.login(user);
    }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            // kiem tra registor error
            if (error.id === 'LOGIN_FAIL') {
                this.setState({ msg: error.msg.msg });
            } else {
                this.setState({ msg: null });
            }
        }

        if (this.state.show) {
            if (this.props.isAuthenticated) {
                this.handleClose();
            }
        }
    }


    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleClose = () => {
        //clear errors
        this.props.clearErrors();
        this.setState({ show: false });
        // console.log(this.state.show)
    }

    handleShow = () => {
        this.setState({ show: true });
        // console.log(this.state.show)
    }

    render() {
        return (
            <div>
                <Nav.Link onClick={() => this.handleShow()}>
                    Đăng nhập
                </Nav.Link>
                {/* <Button
                    onClick={() => this.handleShow()}
                    variant="contained"
                    color="primary"
                    endIcon={<LockOpenIcon href="#"/>}
                >
                    Đăng nhập
                </Button> */}

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>Đăng nhập</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {this.state.msg ? <Alert variant="danger">{this.state.msg}</Alert> : null}

                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email:</Form.Label>
                                <Form.Control type="email" name="email" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Mật khẩu:</Form.Label>
                                <Form.Control type="password" name="password" onChange={(e) => this.changeHandler(e)} />
                            </Form.Group>

                            <Button variant="contained" color="primary" type="submit">Xác nhận</Button>
                        </Form>
                    </Modal.Body>
                </Modal>
            </div>

        )
    }

}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error
})
export default connect(mapStateToProps, { login, clearErrors })(Login);
