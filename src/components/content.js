import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import AdminList from './lists/adminList'
import ProductTypeList from './lists/productTypeList'
import UserList from './lists/userList'
import PromoList from './lists/promoList'
import ProductList from './lists/productList'
import Bill from './bill'
import OrderList from './lists/orderList'
import ProductListAdmin from './lists/productListAdmin'
import Promo from './promo'
import UserProfile from './userProfile'
import History from './history'

//Content hiển thị list dựa theo route
class Content extends Component {

    render() {
        return (
            <Container>
                <Switch>
                    <Redirect exact from="/" to="/home" />
                    <Route exact path="/home" component={ProductList} />
                    <Route exact path="/home/bill" component={Bill} />
                    <Route exact path="/home/profile" component={UserProfile} />
                    <Route exact path="/home/history" component={History} />
                    <Route exact path="/promo" component={Promo} />

                    <Route path="/admin/manager" component={AdminList} />
                    <Route path="/admin/producttype" component={ProductTypeList} />
                    <Route path="/admin/user" component={UserList} />
                    <Route path="/admin/promo" component={PromoList} />
                    <Route path="/admin/product" component={ProductListAdmin} />
                    <Route path="/admin/order" component={OrderList} />
                </Switch>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    // isAuthenticated: state.auth.isAuthenticated,
    // user: state.auth.user
})
export default connect(mapStateToProps, {})(Content);