import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import ProductTypeForm from './forms/productTypeForm'
import PromoForm from './forms/promoForm'
import AdminForm from './forms/adminForm'
import SearchBar from './forms/searchBar'
import ProductForm from './forms/productForm'

class SideBar extends Component {
    render() {

        return (
            <Switch>
                <Route path="/admin/producttype" component={ProductTypeForm} />
                <Route path="/admin/promo" component={PromoForm} />
                <Route path="/admin/manager" component={AdminForm} />
                <Route exact path="/home" component={SearchBar} />
                <Route path="/admin/product" component={ProductForm} />
            </Switch >
        )
    }
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    isAdmin: state.auth.isAdmin,
    user: state.auth.user
})


export default connect(mapStateToProps, {})(SideBar)