import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { getOrderDetails } from '../actions/orderAction'
import { Modal, Form, Nav, Alert } from 'react-bootstrap'
import { Button } from '@material-ui/core'
import { Table } from 'react-bootstrap';

export class OrderDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false
        }
    }

    componentDidMount() {
        const { order_id } = this.props
        this.props.getOrderDetails(order_id)
    }

    handleClose = () => {
        this.setState({ show: false });
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    render() {
        const { orderDetails } = this.props

        const list = (
            <Fragment>
                {orderDetails.map(orderDetail => {
                    const { productName, urlImage } = orderDetail.product;
                    return (
                        <tr>
                            <td>
                                <img src={urlImage} style={{width: '4rem'}}></img>
                            </td>
                            <td>{productName}</td>
                            {/* <td>{productType}</td> */}
                            <td>{orderDetail.quantity}</td>
                            <td>${orderDetail.unitPrice}</td>
                            <td>${orderDetail.unitPrice * orderDetail.quantity}</td>
                        </tr>
                    )
                })}
            </Fragment>
        )

        return (

            <div>
                <Button onClick={() => this.handleShow()} variant='outlined'>
                    Chi tiết
                </Button>

                <Modal show={this.state.show} onHide={() => this.handleClose()} >
                    <Modal.Header closeButton>
                        <Modal.Title>Chi tiết đơn hàng</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Table hover responsive >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Tên sản phẩm</th>
                                    {/* <th>Loại sản phẩm</th> */}
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Tổng</th>
                                </tr>
                            </thead>
                            <tbody>
                                {orderDetails !== [] ? list : <p>Chi tiết đơn hàng trống</p>}
                            </tbody>
                        </Table>
                        <Button onClick={(e) => this.handleClose()} variant='contained' style={{marginTop: '2rem'}}>Trở về</Button>
                    </Modal.Body>
                </Modal>
            </div >
        )
    }
}

const mapStateToProps = (state) => ({
    orderDetails: state.order.orderDetails
})

const mapDispatchToProps = {
    getOrderDetails
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail)
