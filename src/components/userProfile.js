import React, { Component } from 'react'
import { Form, Container, Row, Col, Alert, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
// import PayPalCheckout from './paypal'
import PropTypes from 'prop-types'
import { loadUser } from '../actions/authAction'

class UserProfile extends Component {

    render() {
        return (
            <div>
                {
                    this.props.isAuthenticated ?
                        <Form>
                            <Form.Group controlId="formBasicName">
                                <Form.Label style={{ float: 'left' }}>Tên:</Form.Label>
                                <Form.Control readOnly type="text" name="userName" value={this.props.user.userName} />
                            </Form.Group>

                            <Form.Group controlId="formBasicBirthday">
                                <Form.Label style={{ float: 'left' }}>Ngày sinh:</Form.Label>
                                <Form.Control readOnly type="text" name="birthday" value={this.props.user.birthday} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPhonenumber">
                                <Form.Label style={{ float: 'left' }}>Số điện thoại:</Form.Label>
                                <Form.Control readOnly type="text" name="phone" value={this.props.user.phone} />
                            </Form.Group>

                            <Form.Group controlId="formBasicAddress">
                                <Form.Label style={{ float: 'left' }}>Địa chỉ:</Form.Label>
                                <Form.Control readOnly type="text" name="address" value={this.props.user.address} />
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail">
                                <Form.Label style={{ float: 'left' }}>Email:</Form.Label>
                                <Form.Control readOnly type="email" name="email" value={this.props.user.email} />
                            </Form.Group>
                        </Form>

                        :
                        <Redirect to='/home' />
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    isAuthenticated: state.auth.isAuthenticated
})
const mapDispatchToProps = ({
    loadUser
})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);