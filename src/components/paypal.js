import React, { Component } from 'react';
// import PaypalExpressBtn from 'react-paypal-express-checkout';
import { PayPalButton } from "react-paypal-button-v2";
import { createOrder } from '../actions/orderAction'
import { createOrderDetails } from '../actions/orderDetailAction'
import { removePromoUsed, recordOrderOfUser } from '../actions/authAction'
import { connect } from 'react-redux'
import bson from 'bson-objectid'
import { Redirect } from 'react-router-dom';
import { removeItemsInLocalStorage } from '../actions/cartAction'

class PaypalBtn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            paymentCompleted: false
        }
    }

    async createOrderHandler() {
        const order_id = bson().str
        const { user, orderDetailsInCart, orderDate, shipDate, receiverAddress, note, promoSelected, provisionalSum, total, discount } = this.props
        let promoUsed = null
        if (promoSelected !== null)
            promoUsed = promoSelected._id

        const orderDetails = await this.props.createOrderDetails(orderDetailsInCart, order_id)
        const newOrder = {
            _id: order_id,
            user: user,
            orderDate: orderDate,
            shipDate: shipDate,
            receiverAddress: receiverAddress,
            note: note,
            orderDetails: orderDetails,
            promoUsed: promoUsed,
            provisionalSum: provisionalSum,
            total: total,
            discount: discount
        }
        this.props.createOrder(newOrder)
        this.props.recordOrderOfUser(order_id, user._id)
    }

    render() {
        if (!this.state.paymentCompleted)
            return (
                <PayPalButton
                    amount={this.props.total}
                    // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                    onSuccess={(details, data) => {
                        this.createOrderHandler()
                        if (this.props.promoSelected !== null)
                            this.props.removePromoUsed(this.props.promoSelected._id, this.props.user)
                        alert("Thanh toán thành công!" + details.payer.name.given_name)
                        
                        this.props.removeItemsInLocalStorage()

                        this.setState({ paymentCompleted: true }) //go to '/home'

                        // OPTIONAL: Call your server to save the transaction
                        return fetch("/paypal-transaction-complete", {
                            method: "post",
                            body: JSON.stringify({
                                orderID: data.orderID
                            })
                        });
                    }}
                />
            )
        else
            return <Redirect to='/home' />
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    orderDetailsInCart: state.cart.orderDetailsInCart,
    promoSelected: state.promo.promoSelected,
    provisionalSum: state.cart.provisionalSum,
    total: state.bill.total,
    discount: state.bill.discount
})

const mapDispatchToProps = {
    createOrder,
    createOrderDetails,
    removePromoUsed,
    recordOrderOfUser,
    removeItemsInLocalStorage
}

export default connect(mapStateToProps, mapDispatchToProps)(PaypalBtn);