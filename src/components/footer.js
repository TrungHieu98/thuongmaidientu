import React from 'react';
// import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';

function Copyright() {
    return (
        <Typography variant="body2" style={{ color: 'white' }}>
            {'Libary | '}
            <Link color="inherit" href="https://material-ui.com/">
                Material-UI
            </Link>
            {' | '}
            <Link color="inherit" href="https://react-bootstrap.github.io/">
                React-Bootstrap
            </Link>
            {' | '}
            <Link color="inherit" href="https://developer.paypal.com">
                Paypal Developer
            </Link>
            {' | '}
            <Link color="inherit" href="https://www.mongodb.com/">
                Mongo DB
            </Link>
            {' | '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '40vh',
    },
    main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
    },
    footer: {
        padding: theme.spacing(3, 2),
        marginTop: 'auto',
        backgroundColor: '#202932'
    },
}));

export default function Footer() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <footer className={classes.footer}>
                <Container maxWidth="sm">
                    <Typography variant="body1" style={{ color: 'white' }}>
                        <strong>THƯƠNG MẠI ĐIỆN TỬ</strong>
                    </Typography>
                    <Copyright />
                </Container>
            </footer>
        </div>
    );
}