import {
    ADD_ADMIN_FAIL,
    ADD_ADMIN_SUCCESS,
    GET_ADMINS,
    GET_ADMINS_FAIL,
    DELETE_ADMIN,
    DELETE_ADMIN_FAIL,
    ADMINS_LOADING,
} from '../constants'

const initialState = {
    isAdmin: false,
    admins: [],
    loading: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_ADMIN_SUCCESS:
            return {
                ...state,
                isAdmin: true,
                admins: [action.payload, ...state.admins],
                loading: false
            }
        case ADD_ADMIN_FAIL:
            return {
                ...state
            }
        case GET_ADMINS:
            return {
                ...state,
                isAdmin: true,
                admins: action.payload,
                loading: false
            }
        case GET_ADMINS_FAIL:
            return {
                ...state,
                isAdmin: true,
                loading: false
            }
        case DELETE_ADMIN:
        case DELETE_ADMIN_FAIL:
            return {
                ...state
            }
        case ADMINS_LOADING:
            return {
                ...state,
                isAdmin: true,
                loading: true
            }
        default:
            return state
    }
}