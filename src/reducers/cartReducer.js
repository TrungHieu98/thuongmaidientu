import {
    ADD_ITEM,
    DELETE_ITEM,
    FETCH_ORDER_DETAILS,
    CART_LOADING,
    // CART_CONFIRMED,
    SET_ORDER,
    SET_PROVISONAL_SUM,
    SET_QUANTITY,
    CLEAR_LOCAL_STORAGE,
} from '../constants'

const initialState = {
    isLoading: false,
    items: localStorage.getItem('items'),
    orderDetailsInCart: [],
    order: null,
    provisionalSum: 0
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_ITEM:
            localStorage.setItem('items', JSON.stringify(action.payload))
            return {
                ...state,
                items: localStorage.getItem('items')
            }
        case DELETE_ITEM:
            localStorage.setItem('items', JSON.stringify(action.payload))
            return {
                ...state,
                items: localStorage.getItem('items')
            }
        case FETCH_ORDER_DETAILS:
            return {
                ...state,
                orderDetailsInCart: action.payload,
                isLoading: false
            }
        case CART_LOADING:
            return {
                ...state,
                isLoading: true
            }
        // case CART_CONFIRMED:
        //     return {
        //         ...state,
        //         confirmed: true
        //     }
        case SET_ORDER:
            return {
                ...state,
                order: action.payload
            }
        case SET_PROVISONAL_SUM:
            return {
                ...state,
                provisionalSum: action.payload
            }
        case SET_QUANTITY:
            localStorage.setItem('items', JSON.stringify(action.payload))
            return {
                ...state,
                items: localStorage.getItem('items')
            }
        case CLEAR_LOCAL_STORAGE:
            return {
                ...state,
                items: null,
                orderDetailsInCart: []
            }
        default:

            return state
    }
}