import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    LOGIN_BY_ADMIN,
    USER_LOADED,
    USE_PROMO,
    RECORD_ORDER_OF_USER,
    RECORD_ORDER_OF_USER_FAIL,
    CLEAR_AUTH_PROPS,
} from '../constants'

const initialState = {
    userId: localStorage.getItem('userId'),
    isAdmin: false,
    isAuthenticated: false,
    isLoading: false,
    user: null,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case REGISTER_SUCCESS:
        case LOGIN_SUCCESS:
            localStorage.setItem('userId', action.payload.userId)
            return {
                ...state,
                // ...action.payload,
                isAuthenticated: true,
                isLoading: false,
                user: action.payload,
                isAdmin: false
            }
        case REGISTER_FAIL:
            return {
                ...state
            }
        case LOGOUT:
            localStorage.removeItem('userId')
            return {
                userId: null,
                isAdmin: false,
                isAuthenticated: false,
                isLoading: false,
                user: null,
            }
        case LOGIN_FAIL:
            localStorage.removeItem('userId')
            return {
                ...state,
                isAdmin: false,
                isAuthenticated: false,
                isLoadindg: false,
                user: null
            }

        case LOGIN_BY_ADMIN:
            localStorage.setItem('userId', action.payload._id)
            return {
                ...state,
                isAdmin: true,
                isAuthenticated: true,
                isLoading: false,
                user: action.payload
            }
        case USER_LOADED: {
            // localStorage.setItem('userId', action.payload.userId)
            if (action.payload.priority === 1)
                return {
                    ...state,
                    isAdmin: true,
                    user: action.payload,
                    isAuthenticated: true,
                    isLoading: false,
                }
            else
                return {
                    ...state,
                    user: action.payload,
                    isAdmin: false,
                    isAuthenticated: true,
                    isLoading: false
                }
        }
        case USE_PROMO:
            return {
                ...state,
                user: action.payload
            }
        case RECORD_ORDER_OF_USER:
            return {
                ...state,
                user: action.payload
            }
        case RECORD_ORDER_OF_USER_FAIL:
            return {
                ...state
            }
        default:
            //console.log('default')
            return state
    }
}