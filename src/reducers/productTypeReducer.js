import {
    GET_ALL_TYPE,
    GET_TYPES_FAIL,
    GET_TYPE,
    GET_TYPE_FAIL,
    DELETE_TYPE,
    DELETE_TYPE_FAIL,
    CREATE_TYPE,
    CREATE_TYPE_FAIL,
    UPDATE_TYPE,
    UPDATE_TYPE_FAIL,
    TYPES_LOADING,
    GET_TYPES_EXISTED,
    GET_TYPES_EXISTED_FAIL
} from '../constants';

const initialState = {
    productType: null,
    productTypes: [],
    loading: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ALL_TYPE:
            return {
                ...state,
                productTypes: action.payload,
                loading: false
            }
        case GET_TYPES_FAIL:
            return {
                ...state
            }
        case GET_TYPE:
            return {
                ...state,
                productType: action.payload
            }
        case GET_TYPE_FAIL:
        case UPDATE_TYPE:
            return {
                ...state,
                productType: action.payload
            }
        case UPDATE_TYPE_FAIL:
        case DELETE_TYPE:
        case DELETE_TYPE_FAIL:
            return {
                ...state
            }
        case CREATE_TYPE:
            return {
                ...state,
                productType: action.payload
            }
        case TYPES_LOADING:
            return {
                ...state,
                loading: true
            }
        case GET_TYPES_EXISTED:
            return {
                ...state,
                productTypes: action.payload
            }
        case GET_TYPES_EXISTED_FAIL:
            return {
                ...state,
            }

        default:
            return state
    }
}