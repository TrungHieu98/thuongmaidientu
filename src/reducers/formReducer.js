import {
    SET_CREATING,
    SET_UPDATING
} from '../constants'

const initialState = {
    updating: false,
    id: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CREATING:
            return {
                update: false,
                id: null
            }
        case SET_UPDATING:
            return {
                updating: true,
                id: action.payload
            }
        default:
            return state;
    }
}