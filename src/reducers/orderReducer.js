import { CREATE_ORDER, CREATE_ORDER_FAIL, GET_ORDERS, GET_ORDER, GET_ORDERS_FAIL, GET_ORDERS_HISTORY, GET_ORDERS_HISTORY_FAIL, GET_ORDER_DETAILS_FROM_ORDER, GET_ORDER_DETAILS_FROM_ORDER_FAIL } from '../constants';

const initialState = {
    order: null,
    orders: [],
    isLoading: false,
    ordersHistory: [],
    orderDetails: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_ORDER:
            return {
                ...state,
            }
        case CREATE_ORDER_FAIL:
            return {
                ...state,
            }
        case GET_ORDER:
            return {
                ...state,
                order: action.payload
            }
        case GET_ORDERS:
            return {
                ...state,
                orders: action.payload
            }
        case GET_ORDERS_FAIL:
            return {
                ...state,
            }
        case GET_ORDERS_HISTORY:
            return {
                ...state,
                ordersHistory: action.payload
            }
        case GET_ORDERS_HISTORY_FAIL:
            return {
                ...state
            }
        case GET_ORDER_DETAILS_FROM_ORDER:
            return {
                ...state,
                orderDetails: action.payload
            }
        case GET_ORDER_DETAILS_FROM_ORDER_FAIL:
            return {
                ...state
            }
        default:
            return state
    }
}