import {
    GET_USERS,
    GET_USERS_FAIL,
    DELETE_USER,
    DELETE_USER_FAIL,
    USERS_LOADING,
    UPDATE_USER,
    UPDATE_USER_FAIL,
    GET_USER,
    TAKE_PROMO,
    TAKE_PROMO_FAIL,
    USE_PROMO
} from '../constants'

const initialState = {
    user: null,
    users: [],
    loading: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_USERS:
            return {
                ...state,
                users: action.payload,
                loading: false
            }
        case GET_USER:
            return {
                ...state,
                user: action.payload
            }
        case USERS_LOADING:
            return {
                ...state,
                loading: true
            }
        case UPDATE_USER:
            return {
                ...state,
                user: action.payload
            }
        case GET_USERS_FAIL:
        case DELETE_USER:
        case DELETE_USER_FAIL:
            return {
                ...state,
                loading: false,
                users: []
            }
        case UPDATE_USER_FAIL:
            return {
                ...state,
                user: null
            }
        
        default:
            return state
    }
}