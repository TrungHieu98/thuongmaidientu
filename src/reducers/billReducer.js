import { SET_DISCOUNT_VALUE, SET_TOTAL, RESET_DISCOUNT_VALUE } from '../constants';

const initialState = {
    total: 0,
    discount: 0
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_DISCOUNT_VALUE:
            return {
                ...state,
                discount: action.payload
            }
        case SET_TOTAL:
            return {
                ...state,
                total: action.payload
            }
        case RESET_DISCOUNT_VALUE:
            return {
                ...state,
                discount: action.payload
            }
        default:
            return state;
    }
}