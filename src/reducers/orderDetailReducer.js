import { CREATE_ORDER_DETAIL, CREATE_ORDER_DETAIL_FAIL } from "../constants"

const initialState = {
    orderDetail: null,
    orderDetails: [],
    isLoading: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_ORDER_DETAIL:
            return {
                ...state
            }
        case CREATE_ORDER_DETAIL_FAIL:
            return {
                ...state
            }
        default:
            return state
    }
}