import { combineReducers } from 'redux'

import authReducer from './authReducer'
import errorReducer from './errorReducer'
import adminReducer from './adminReducer'
import productTypeReducer from './productTypeReducer'
import userReducer from './userReducer'
import promoReducer from './promoReducer'
import productReducer from './productReducer'
import formReducer from './formReducer'
import cartReducer from './cartReducer'
import orderReducer from './orderReducer'
import orderDetailReducer from './orderDetailReducer'
import billReducer from './billReducer'

export default combineReducers({
    auth: authReducer,
    admin: adminReducer,
    error: errorReducer,
    productType: productTypeReducer,
    user: userReducer,
    promo: promoReducer,
    product: productReducer,
    form: formReducer,
    cart: cartReducer,
    order: orderReducer,
    orderDetail: orderDetailReducer,
    bill: billReducer
})