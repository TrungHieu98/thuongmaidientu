import {
    GET_PRODUCTS_BY_ADMIN,
    GET_PRODUCTS_FAIL,
    GET_PRODUCT,
    GET_PRODUCT_FAIL,
    DELETE_PRODUCT,
    DELETE_PRODUCT_FAIL,
    CREATE_PRODUCT,
    CREATE_PRODUCT_FAIL,
    PRODUCTS_LOADING,
    GET_PRODUCTS_WITH_FILTER_FAIL,
    GET_PRODUCTS_WITH_FILTER,
    GET_PRODUCTS_BY_USER,
    GET_PRODUCTS_BY_USER_FAIL
} from '../constants';

const initialState = {
    product: null,
    products: [],
    productsFiltered: [],
    loading: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCTS_BY_ADMIN:
            return {
                ...state,
                products: action.payload,
                loading: false
            }
        case GET_PRODUCTS_FAIL:
            return {
                ...state,
                products: null,
                loading: false
            }
        case GET_PRODUCT:
            return {
                ...state,
                product: action.payload
            }
        case GET_PRODUCT_FAIL:
        case DELETE_PRODUCT:
        case DELETE_PRODUCT_FAIL:
            return {
                ...state
            }
        case CREATE_PRODUCT:
        case CREATE_PRODUCT_FAIL:
            return {
                ...state
            }
        case PRODUCTS_LOADING:
            return {
                ...state,
                loading: true
            }
        case GET_PRODUCTS_WITH_FILTER:
            return {
                ...state,
                productsFiltered: action.payload
            }
        case GET_PRODUCTS_WITH_FILTER_FAIL:
            return {
                ...state,
            }
        case GET_PRODUCTS_BY_USER:
            return {
                ...state,
                products: action.payload
            }
        case GET_PRODUCTS_BY_USER_FAIL:
            return {
                ...state
            }
        default:
            return state
    }
}